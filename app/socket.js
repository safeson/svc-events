/**
 * Wrapper do websocket
 */
exports = module.exports = function(ws, config) {
    function WebSocket(cfg) {
        let attr = {
            port: (cfg ? cfg.PORT : config.PORT) || 9999,
            perMessageDeflate: {
                zlibDeflateOptions: { // See zlib defaults.
                    chunkSize: 1024,
                    memLevel: 7,
                    level: 3,
                },
                zlibInflateOptions: {
                    chunkSize: 10 * 1024
                },
                // Other options settable:
                clientNoContextTakeover: true,
                 // Defaults to negotiated value.
                serverNoContextTakeover: true, // Defaults to negotiated value.
                clientMaxWindowBits: 10,       // Defaults to negotiated value.
                serverMaxWindowBits: 10,       // Defaults to negotiated value.
                // Below options specified as default values.
                concurrencyLimit: 10,          // Limits zlib concurrency for perf.
                threshold: 1024,               // Size (in bytes) below which messages
            }
        };
    
        this.server = new ws.Server(attr);
    }

    /**
     * Send message to everyone connected in socket
     */
    WebSocket.prototype.broadcast = function(data) {
        this.server.clients.forEach((client) => {
            if (client.readyState === ws.OPEN) {
                client.send(data);
            }
        });
    } 

    return WebSocket;
}

exports['@singleton'] = false;
exports['@require'] = ['ws', 'configurator'];
