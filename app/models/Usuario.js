/**
 * Usuario Model
 */

exports = module.exports = function() {
    function Usuario(args) {
        this.id = args.id;
        this.nome = args.nome || args.name;
        this.email = args.email;
        this.imgUrl = args.picture || args.img_url;
        this.socialId = args.sub || args.id_social;
        this.created = !!args.created;
    }

    return Usuario;
}

exports['@singleton'] = false;   
exports['@require'] = [];