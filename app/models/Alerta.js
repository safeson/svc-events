/**
 * Alerta Model
 */

exports = module.exports = function(Coordenada) {
    function Alerta(args) {
        this.id = args.alertId;

        // Data de expiração
        this.duracao = args.duracao;
        this.dataCriacao = args.data_criacao;
        this.coordenada = args.latitude ? new Coordenada(
            null, args.latitude, args.longitude
        ) : new Coordenada().set(args.coordenada);
        this.descricao = args.descricao;
        this.nome = args.nome;
        this.tipoId = args.alertaId;
    }

    return Alerta;
}

exports['@singleton'] = false;   
exports['@require'] = ['models/Coordenada'];