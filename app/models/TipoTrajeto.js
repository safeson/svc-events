/**
 * TipoTrajeto Model
 */

exports = module.exports = function() {
    function TipoTrajeto(descricao, modo) {
        this.descricao = descricao;
        this.modo = modo;
    }

    TipoTrajeto.prototype.set = function(args) {
        this.descricao = args.descricao;
        this.modo = args.modo;

        return this;
    }

    return TipoTrajeto;
}

exports['@singleton'] = false;   
exports['@require'] = [];