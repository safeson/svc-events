/**
 * Coordenada Model
 */

exports = module.exports = function() {
    function Coordenada(local, latitude, longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.local = local;
    }

    Coordenada.prototype.set = function(args) {
        this.latitude = args.latitude;
        this.longitude = args.longitude;
        this.local = args.local;

        return this;
    }

    return Coordenada;
}

exports['@singleton'] = false;   
exports['@require'] = [];