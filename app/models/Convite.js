/**
 * Convite Model
 */

exports = module.exports = function() {
    function Convite(data, uuid, user) {
        this.data_enviado = data;
        this.trajetoUuid = uuid;
        this.usuario = user;
    }

    return Convite;
}

exports['@singleton'] = false;   
exports['@require'] = [];