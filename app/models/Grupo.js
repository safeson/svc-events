/**
 * Alerta Model
 */

exports = module.exports = function() {
    function Grupo(args) {
        this.id = args.id;

        this.membros = args.membros || args.membrosString.split(';');
        this.nome = args.nome;
        this.socialId = args.socialId;
    }

    return Grupo;
}

exports['@singleton'] = false;   
exports['@require'] = [];