/**
 * Trajeto Model
 */

exports = module.exports = function(Coordenada, TipoTrajeto, Usuario) {
    function Trajeto(args) {
        this.dataInicio = args.data_inicio || new Date();
        this.uuid = args.uuid;
        this.bateria = args.bateria || args.bateria_inicio;
        this.avaliacao = args.avaliacao;
        this.duracao = args.duracao || args.duracao_estimada;
        this.finalizado = Boolean(args.finalizado);
        try {
            this.pontos = JSON.parse(args.pontos);
        } catch(err) {
            console.log("Json problem -> " + err);
        }

        this.tipoTrajeto = new TipoTrajeto(args.descricao, args.modo);
        if (args.tipoTrajeto)
            this.tipoTrajeto = this.tipoTrajeto.set(args.tipoTrajeto);

        this.partida = new Coordenada(args.pLocal, args.pLat, args.pLong);
        if (args.partida)
            this.partida = this.partida.set(args.partida);

        this.destino = new Coordenada(args.dLocal, args.dLat, args.dLong);
        if (args.destino)
            this.destino = this.destino.set(args.destino);

        if (args.criador)
            this.criador = new Usuario(args.criador);
    }

    return Trajeto;
}

exports['@singleton'] = false;
exports['@require'] = ['models/Coordenada', 'models/TipoTrajeto', 'models/Usuario'];