/**
 * Panico Model
 */

exports = module.exports = function(Coordenada) {
    function Panico(args) {
        this.id = args.id;

        this.coordenada = args.latitude ? new Coordenada(
            null, args.latitude, args.longitude
        ) : new Coordenada().set(args.coordenada);

        this.trajetoUuid = args.trajetoUuid;
        this.data = args.data;
        this.socialId = args.socialId;
    }

    return Panico;
}

exports['@singleton'] = false;   
exports['@require'] = ['models/Coordenada'];