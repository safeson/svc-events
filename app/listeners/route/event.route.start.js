exports = module.exports = function(BaseListener, RouteManager, InviteManager) {
    const broadcastName = 'route.invites';

    function execute() {
        console.log('Executing event.route.start: \nData received ->');
        console.log(this.data);

        let that = this;
        
        if (!this.data.criador || !this.data.partida 
            || !this.data.destino || !this.data.tipoTrajeto
            || !this.data.pontos)
            return this.missingParams(); 

        RouteManager.start(this.data, (err, trajeto, convidados) => {
            if (err) {
                console.error(`Erro criando trajeto -> ${err}`);
                return that.error("Criação de trajeto invalido");
            }

            convidados.forEach(convidado => {
                InviteManager.getInviteTrajeto(convidado.socialId, trajeto.uuid, (err, convite) => {
                    if (err) {
                        console.error(`Falha ao enviar convite do trajeto (${trajeto.uuid}) para usuario -> ${convidadoSocialId}`);
                        return;
                    }

                    that.broadcastTo(convidado.socialId, convite, broadcastName);
                });
            });
            
            that.success(trajeto);
        });        
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'route/routeManager', 'invite/inviteManager'];