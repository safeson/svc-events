exports = module.exports = function(BaseListener, Coordenada, routeManager) {
    const name = 'my.coordinates';
    const finish = 'route.finish';

    function execute() {
        console.log('Executing broadcast.my.coordinates: \nData sent ->');
        console.log(this.data);
        
        let that = this;

        if (!this.data.coordenada)
            return this.missingParams();

        routeManager.verifyFinish(this.ws.key, this.data.coordenada, (err, finished) => {
            if (err) {
                console.error("Erro verificando finalização da rota: " + err);
                return;
            }

            if (!finished) return;

            const finishData = {
                tipoFim: 'CONCLUIDO',
                bateria: that.data.bateria,
                qtdAssistindo: that.ws.clients.size - 1,
                routeUuid: that.ws.key
            }; 

            console.log("Finalizando trajeto -> " + that.ws.key);

            routeManager.finish(finishData, (err) => {
                if (err) {
                    console.error("Erro finalizando trajeto: " + err);
                    return;
                }
                that.broadcast({}, finish);
                that.ws.finish();
            });           
        });

        this.ws.clients.forEach(cli => {
            if (that.ws.key === cli.key && cli.readyState === cli.OPEN) {
                cli.route.lastLocation = that.data;
            }
        });

        // returned all ok
        this.broadcast(this.data, name);
    }

    return new BaseListener(execute);
}
    
exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'models/Coordenada', 'route/routeManager'];