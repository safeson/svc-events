exports = module.exports = function(BaseListener, InviteManager) {
    /**
     * Params required:
     * socialId, trajetoUuid, aceito
     */
    function execute() {
        console.log('Executing event.invite.respond: \nData received ->');
        console.log(this.data);

        let that = this;

        if (!this.data.trajetoUuid || !this.data.socialId)
            return this.missingParams(); 

        InviteManager.respond(this.data, (err) => {
            if (err) {
                console.error(`Erro respondendo convite -> ${err}`);
                return that.error("Erro ao responder convite");
            }

            that.success();
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'invite/inviteManager'];