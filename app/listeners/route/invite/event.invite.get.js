exports = module.exports = function(BaseListener, InviteManager) {
    /**
     * Params required:
     * socialId
     */
    function execute() {
        console.log('Executing event.invite.get: \nData received ->');
        console.log(this.data.socialId);

        let that = this;

        if (!this.data.socialId)
            return this.missingParams(); 

        InviteManager.getInvites(this.data.socialId, (err, invites) => {
            if (err) {
                console.error(`Erro recebendo convites -> ${err}`);
                return that.error("Erro ao receber convites abertos");
            }

            that.success(invites);
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'invite/inviteManager'];