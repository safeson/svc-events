exports = module.exports = function(BaseListener, RouteManager) {
    const finish = 'route.finish';
    
    /**
     * Params required:
     * Bateria (porcentagem da bateria)
     */
    function execute() {
        console.log('Executing event.route.finish: \nData received ->');
        console.log(this.data);

        let that = this;

        this.data.qtdAssistindo = this.ws.clients.size - 1;
        this.data.routeUuid = this.ws.key;

        if (this.ws.observer) {
            return that.error("Você não pode finalizar esse trajeto");
        }

        if (!this.data.routeUuid)
            return this.missingParams();

        RouteManager.finish(this.data, (err) => {
            if (err) {
                console.error(`Erro finalizando trajeto -> ${err}`);
                return that.error("Não foi possível finalizar trajeto");
            }

            console.log(`[${that.data.routeUuid}] Rota finalizada com sucesso`);

            that.success();
            that.broadcast({}, finish);
            that.ws.finish();
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'route/routeManager'];