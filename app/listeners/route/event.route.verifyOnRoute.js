exports = module.exports = function(BaseListener, RouteManager) {
    /**
     * Params required:
     * coordenada, data, trajetoUuid, onRoute
     */
    function execute() {
        console.log('Executing event.route.verifyOnRoute: \nData received ->');
        console.log(this.data);

        let that = this;
        
        if (!this.data.data || !this.data.coordenada || !this.data.trajetoUuid)
            return this.missingParams(); 

        if (!this.ws.route.criador.myself) 
            return this.error('Você não é o criador da rota');

        if (this.ws.isOnRoute && this.data.onRoute)
            return this.success();

        if (this.ws.isOnRoute && !this.data.onRoute) {
            RouteManager.userLeft(this.data, (err) => {
                if (err) return that.error(err);

                that.success();
            });
        }

        if (!this.ws.isOnRoute && this.data.onRoute) {
            RouteManager.userBack(this.data, (err) => {
                if (err) return that.error(err);

                that.success();
            });
        }

        this.ws.isOnRoute = this.data.onRoute;        
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'route/routeManager'];