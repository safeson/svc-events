exports = module.exports = function(BaseListener, RouteManager) {
    /**
     * Params required:
     * socialId
     */
    function execute() {
        console.log('Executing event.route.getMyOpenRoute: \nData received ->');
        console.log(this.data.socialId);

        let that = this;
        
        if (!this.data.socialId)
            return this.missingParams(); 

        RouteManager.getMyOpenRoute(this.data.socialId, (err, route) => {
            if (err) {
                console.error(`Erro recebendo rota aberta -> ${err}`);
                return that.error("Erro recebendo rota aberta do usuário");
            }

            that.success(route);
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'route/routeManager'];