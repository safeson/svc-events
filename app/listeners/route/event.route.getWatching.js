exports = module.exports = function(BaseListener, RouteManager) {
    /**
     * Params required:
     * socialId
     */
    function execute() {
        console.log('Executing event.route.getWatching: \nData received ->');
        console.log(this.data.socialId);

        let that = this;
        
        if (!this.data.socialId)
            return this.missingParams(); 

        RouteManager.getWatching(this.data.socialId, (err, routes) => {
            if (err) {
                console.error(`Erro recebendo rotas -> ${err}`);
                return that.error("Erro nas rotas para observar");
            }

            that.success(routes);
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'route/routeManager'];