exports = module.exports = function(BaseListener, panicManager) {
    const name = 'panic.add';

    /**
     * Required params
     * socialId, coordenada, trajetoUuid
     */

    function execute() {
        console.log('Executing PANICO: \nData sent ->');
        console.log(this.data);

        if (!this.data.trajetoUuid || !this.data.socialId || !this.data.coordenada)
            return this.missingParams();

        panicManager.add(this.data, (err, panico) => { 
            if (err) {
                console.error(`Erro executando panico -> ${err}`);
                return that.error("Não foi possível executar o panico");
            }

            this.broadcast(panico, name);
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'panic/panicManager'];