/**
 * Recebe os alertas
 */

exports = module.exports = function(BaseListener, AlertManager) {
    function execute() {
        console.log('Executing event.alert.get: \nData received ->');
        console.log('Requesting alerts');

        let that = this;

        AlertManager.get((err, alerts) => {
            if (err) {
                console.error(`Event alerts get err -> ${err}`);
                return that.error("Falha ao receber os alertas");
            }

            that.success(alerts);
        });        
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'alert/alertManager'];