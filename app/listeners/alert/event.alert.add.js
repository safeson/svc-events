exports = module.exports = function(BaseListener, AlertManager) {
    /**
     * Params required:
     * coordenada, alertaId, socialId
     */

    const broadcastName = 'alert.add';

    function execute() {
        console.log('Executing event.alert.add: \nData received ->');
        console.log(this.data);

        let that = this;
        
        if (!this.data.coordenada || !this.data.alertaId || !this.data.socialId)
            return this.missingParams(); 

        AlertManager.add(this.data, (err, alert) => {
            if (err) {
                console.error(`Erro adicionando um alerta -> ${err}`);
                return that.error("Não foi possível adicionar um alerta");
            }

            that.broadcast(alert, broadcastName);
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'alert/alertManager'];