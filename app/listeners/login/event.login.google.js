exports = module.exports = function(BaseListener, AuthManager) {
    function execute() {
        console.log('Executing event.login.google: \nData received ->');
        console.log(this.data.idToken);

        let that = this;

        if (!this.data.idToken)
            return this.missingParams(); 

        AuthManager.loginGoogle(this.data.idToken, (err, user) => {
            if (err) {
                console.error(`Login google failed -> ${err}`);
                return that.error("Usuário inválido!");
            }

            // Salva o socialId do usuario na conexão
            that.ws.socialId = user.socialId;

            that.success(user);
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'auth/authManager'];