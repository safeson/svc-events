/**
 * Recebe os amigos de um grupo
 */

exports = module.exports = function(BaseListener, groupManager) {
    function execute() {
        console.log('Executing event.group.update');

        let that = this;

        if (!this.data.id || !this.data.socialId || !this.data.nome)
            return this.missingParams();

        groupManager.remove(this.data.id, (err) => {
            if (err) {
                console.error(`Event group remove -> ${err}`);
                return that.error("Falha ao remover grupo");
            }

            groupManager.add(this.data, (err, grupo) => {
                if (err) {
                    console.error(`Event group update -> ${err}`);
                    return that.error("Falha ao atualizar um grupo");
                }
    
                that.success(grupo);
            });
        });             
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'group/groupManager'];