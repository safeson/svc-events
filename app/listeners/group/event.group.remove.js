/**
 * Recebe os amigos de um grupo
 */

exports = module.exports = function(BaseListener, groupManager) {
    function execute() {
        console.log('Executing event.group.get');

        let that = this;

        if (!this.data.id)
            return this.missingParams();

        groupManager.remove(this.data.id, (err) => {
            if (err) {
                console.error(`Event group remove -> ${err}`);
                return that.error("Falha ao remover grupo");
            }

            that.success();
        });        
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'group/groupManager'];