/**
 * Recebe os amigos de um grupo
 */

exports = module.exports = function(BaseListener, groupManager) {
    function execute() {
        console.log('Executing event.group.add');

        let that = this;

        if (!this.data.socialId || !this.data.nome)
            return this.missingParams();

        groupManager.add(this.data, (err, grupo) => {
            if (err) {
                console.error(`Event group add -> ${err}`);
                return that.error("Falha ao adicionar um grupo");
            }

            that.success(grupo);
        });        
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'group/groupManager'];