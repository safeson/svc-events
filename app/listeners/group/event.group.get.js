/**
 * Recebe os amigos de um grupo
 */

exports = module.exports = function(BaseListener, groupManager) {
    function execute() {
        console.log('Executing event.group.get');

        let that = this;

        if (!this.data.socialId)
            return this.missingParams();

        groupManager.get(this.data.socialId, (err, grupos) => {
            if (err) {
                console.error(`Event group get -> ${err}`);
                return that.error("Falha ao receber grupos");
            }

            that.success(grupos);
        });        
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'group/groupManager'];