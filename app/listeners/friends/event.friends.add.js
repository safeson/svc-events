/**
 * Recebe os amigos ou um amigo especifico
 */

exports = module.exports = function(BaseListener, FriendsManager) {
    function execute() {
        console.log('Executing event.friends.add: \nData received ->');
        console.log(`Adding friend ${this.data.email} for user: ${this.data.socialId} via email`);

        let that = this;

        if (!this.data.socialId || !this.data.email)
            return this.missingParams(); 

        FriendsManager.add(this.data.socialId, this.data.email, (err, friend) => {
            if (err) {
                console.error(`Event friends add err -> ${err}`);
                return that.error("Falha ao adicionar um amigo");
            }

            that.success(friend);
        });
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'friends/friendsManager'];