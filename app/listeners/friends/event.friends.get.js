/**
 * Recebe os amigos ou um amigo especifico
 */

exports = module.exports = function(BaseListener, FriendsManager) {
    function execute() {
        console.log('Executing event.friends.get: \nData received ->');
        console.log('Requesting friends for: ' + this.data.socialId);

        let that = this;

        FriendsManager.get(this.data.socialId, this.data.friendId, (err, friends) => {
            if (err) {
                console.error(`Event friends get err -> ${err}`);
                return that.error("Falha ao receber amigos");
            }

            that.success(friends);
        });        
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'friends/friendsManager'];