/**
 * Recebe os amigos ou um amigo especifico
 */

exports = module.exports = function(BaseListener, FriendsManager) {
    function execute() {
        console.log('Executing event.friends.remove: \nData received ->');
        console.log('Deleting friend -> ' + this.data.friendId + ' -> ' + this.data.socialId);

        let that = this;

        if (!this.data.friendId || !this.data.socialId)
            return this.missingParams(); 

        FriendsManager.remove(this.data.socialId, this.data.friendId, (err) => {
            if (err) {
                console.error(`Event friends remove err -> ${err}`);
                return that.error("Falha ao remover um amigo");
            }

            that.success();
        });        
    }

    return new BaseListener(execute);
}

exports['@singleton'] = false;
exports['@require'] = ['BaseListener', 'friends/friendsManager'];