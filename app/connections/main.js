

/**
 * Inicializa a conexão principal do aplicativo
 */
exports = module.exports = function(WSocket, messageListener) {
    return () => {
        const ws = new WSocket();
        const wsServer = ws.server;
        
        wsServer.on('connection', (ws, req) => {
            const ip = (req.headers['x-forwarded-for'] && req.headers['x-forwarded-for'].split(/\s*,\s*/)[0]) 
                || req.connection.remoteAddress;
        
            ws.key = req.url.replace('/', '');
            ws.isAlive = true;
            ws.clients = wsServer.clients;
        
            messageListener(ws);
        
            ws.on('pong', () => {
                ws.isAlive = true;
            });
        
            console.log('Nova conexão -> IP: ' + ip);
        });
        
        setInterval(() => {
            wsServer.clients.forEach((ws, cli) => {
                if (!ws.isAlive) {
                    console.log("Desconectou -> " + cli);
                    return ws.terminate();
                }
                
                ws.isAlive = false;
                ws.ping(null, false, true);
            });
        }, 3000);
    }
}

exports['@singleton'] = true;
exports['@require'] = ['socket', 'messageListener'];
