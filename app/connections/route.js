

/**
 * Inicializa a conexão das rotas (trajetos) do aplicativo
 */
exports = module.exports = function(WSocket, messageListener, routeConfig, RouteManager, resultManager) {
    return () => {
        const ws = new WSocket(routeConfig);
        const wsServer = ws.server;

        const msgBroadcastRota = 'route.connected';
        const msgBroadcastUserLeft = 'route.userLeft';
    
        wsServer.on('connection', (ws, req) => {
            const params = req.url.replace('/', '').split('/');
            
            ws.key = params[1];
            ws.socialId = params[2];

            // For Guest
            ws.ip = (req.headers['x-forwarded-for'] && req.headers['x-forwarded-for'].split(/\s*,\s*/)[0])
                || req.connection.remoteAddress;
            ws.agent = req.headers['user-agent'];
            ws.isAlive = true;
            ws.clients = wsServer.clients;
            ws.notOnRoute = 0;
            ws.isOnRoute = true;

            // Trata usuários duplicados (conectados duas vezes)
            wsServer.clients.forEach(cli => {
                if (cli != ws
                    && cli.socialId 
                    && cli.socialId === ws.socialId 
                    && ws.key === cli.key
                    && cli.readyState === cli.OPEN) {
                        cli.isAlive = false;
                    }
            });

            ws.finish = function() {
                console.log(`[${ws.key}] Conexão finalizada, desconectando usuários...`);
                wsServer.clients.forEach((cli) => {
                    if (ws.key === cli.key) {
                        cli.isAlive = false;
                    }
                    return cli;
                });
            }

            const sendTo = function(data, name) {
                const dataToBroadcast = resultManager.broadcast(data, name);
    
                ws.send(dataToBroadcast);
            }

            RouteManager.connect(ws, (err) => {
                if (err) {
                    console.error(`Falha ao conectar-se com a Rota -> ${ws.key} -> err: ${err}`);
                    ws.isAlive = false;
                    return;
                }

                console.log(`[${ws.key}] Usuário conectado trajeto, ip -> ${ws.ip}; socialId -> ${ws.socialId}; criador da rota? -> ${!ws.observer}`);

                messageListener(ws, true);

                wsServer.clients.forEach((cli) => {
                    if (ws.key === cli.key) {
                        ws.route.lastLocation = cli.route.lastLocation;
                    }
                });

                sendTo(ws.route, msgBroadcastRota);
            });
    
            ws.on('pong', () => {
                ws.isAlive = true;
            });
        });

        setInterval(() => {
            wsServer.clients.forEach((ws, cli) => {
                if (!ws.socialId && ws.observer) {
                    RouteManager.disconnectGuest(ws.key, ws.observer, (err) => {
                        if (err) {
                            console.error(`Erro ao desconectar usuário: ip -> ${ws.ip}; socialId -> ${ws.socialId}`);
                            return;
                        }
                    });
                }

                if (ws.route && ws.route.criador && ws.route.criador.myself) {
                    if (ws.isOnRoute) {
                        ws.notOnRoute = 0;
                    } else {
                        ws.notOnRoute++;
                    }

                    // Depois de 1 minuto (pois 3 segundos * 20 é 1 minuto)
                    // Envia a todos os observadores que o criador da rota saiu da rota
                    if (ws.notOnRoute > 20) {
                        console.log(`[${ws.key}] Criador fora da rota por muito tempo, enviando mensagem para observadores...`);
                        
                        ws.notOnRoute = 0;
                        
                        const dataToBroadcast = resultManager.broadcast({}, msgBroadcastUserLeft);
                        wsServer.clients.forEach((cli) => {
                            if (cli.key === ws.key
                                && cli.socialId !== ws.socialId) {
                                cli.send(dataToBroadcast);
                            }
                        });
                        
                    }
                }

                if (!ws.isAlive) {
                    console.log(`[${ws.key}] Desconectou: ip -> ${ws.ip}; socialId -> ${ws.socialId};`);
                    ws.terminate();
                    return;
                }
                
                ws.isAlive = false;
                ws.ping(null, false, true);
            });
        }, 3000);
    };

}

exports['@singleton'] = true;
exports['@require'] = ['socket', 'messageListener', 'routeConfig', 'route/routeManager', 'resultManager'];
