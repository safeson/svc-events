/**
 * Configurator
 */
exports = module.exports = function(dev, prod, process) {
    const CONFIGURATIONS = {
        development: dev,
        production: prod,
    };
    
    function Configurator() {
        const conf = process.env.NODE_ENV || 'development';
    
        Object.assign(this, CONFIGURATIONS[conf]);
    }
    
    return new Configurator();
}

exports['@singleton'] = true;
exports['@require'] = ['development', 'production', 'process'];