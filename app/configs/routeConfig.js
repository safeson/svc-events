exports = module.exports = function() {
    const CONSTANTS = {
        PORT: 8888
    };
    
    return CONSTANTS;
}

exports['@singleton'] = true;
exports['@require'] = [];