/**
 * Inicializa o aplicativo
 */
exports = module.exports = function(mainConnection, routeConnection) {

    mainConnection();
    routeConnection();

}

exports['@singleton'] = true;
exports['@require'] = ['connections/main', 'connections/route'];
