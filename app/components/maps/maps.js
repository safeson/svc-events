/**
 * Maps manager
 */
exports = module.exports = function(configs) {
    const API = require('@google/maps');
    const googleMapsClient = API.createClient({
        key: configs.MAPS
    });

    return googleMapsClient;
}

exports['@singleton'] = true;
exports['@require'] = ['configurator'];