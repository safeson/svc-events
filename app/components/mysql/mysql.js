/**
 * Wrapper do MYSQL2 - Objeto MySQL
 */
exports = module.exports = function(pool, mysql2, settings) {
	function abreConexao(cb) {
		pool.get().getConnection(function(err, con) {
			if (err) {
				console.error(`Erro de conexão com a pool do mysql -> ${err}`, 'mysql');
				return cb(err);
			}

			cb(null, con);
		});
	}

	function abreConexaoMultipla() {
		return mysql2.createConnection({
			host: settings.MYSQL.HOST,
			user: settings.MYSQL.USER,
			password: settings.MYSQL.PASSWORD,
			database: settings.MYSQL.DB_NAME,
			multipleStatements: true,
		});
	}

	function executaQuery(query, escapes, cb) {
		abreConexao(function(err, con) {
			if (err) return cb(err);

			con.query(query, escapes, function(error, results, fields) {
				con.release();

				if (error) {
					console.error(`Erro na execução: query -> ${query} err -> ${error}`, 'mysql');
					return cb(error);
				}

				cb(null, results, fields);
			});
		});
	};

	function fechaConexao(con, cb) {
		if (!con) return cb('Não há conexão aberta');

		pool.get().releaseConnection(con);
		cb();
	}

	return {
		executa: executaQuery,

		abreConexaoMultipla: abreConexaoMultipla,
		abreConexao: abreConexao,
		fechaConexao: fechaConexao,
	}
}

exports['@singleton'] = false;
exports['@require'] = ['mysqlPool', 'mysql2', 'configurator'];