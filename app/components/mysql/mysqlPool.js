exports = module.exports = function(settings, mysql2) {
	function MysqlPool() {
		console.log('Criado MySQL Pool para database -> ' + settings.MYSQL.DB_NAME, 'MYSQL');
		this.pool = mysql2.createPool({
			host: settings.MYSQL.HOST,
			user: settings.MYSQL.USER,
			password: settings.MYSQL.PASSWORD,
			database: settings.MYSQL.DB_NAME,
			waitForConnections: true,
			queueLimit: 0,
			connectTimeout: 100000
		});
	}

	MysqlPool.prototype.get = function() {
		return this.pool;
	}

	return new MysqlPool();
}

exports['@singleton'] = true;
exports['@require'] = ['configurator', 'mysql2'];