/**
 * Authentication manager
 * Handle events for facebook and google auth
 */

exports = module.exports = function(AuthLibrary, configs, Database) {

    const client = new AuthLibrary.OAuth2Client(configs.AUTH_CLIENT_ID);

    function loginGoogle(token, cb) {
        client.verifyIdToken({
            idToken: token,
            audience: configs.AUTH_CLIENT_ID,
        }, (err, ticket) => {
            if (err || !ticket) return cb(err || "Ticket invalido");

            const payload = ticket.getPayload();

            Database.verifyAndCreate(payload, (err, user) => {
                if (err) return cb(err);

                cb(null, user);
            });
        });
    }

    return {
        loginGoogle: loginGoogle
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['google-auth-library', 'configurator', 'auth/authDatabase'];