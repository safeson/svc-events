/**
 * Auth Database manager
 * Handle database connection for Authentication methods
 */

exports = module.exports = function(mysql, Usuario) {

    /**
     * Verify if user exists, if not create it
     * @returns User Model
     */
    function verifyAndCreate(payload, cb) {
        const query = `
            SELECT * FROM usuario WHERE id_social = ?
        `;

        mysql.executa(query, [payload.sub], (err, data) => {
            if (err) return cb(err);

            const user = data[0];

            if (!user) return create(payload, cb);

            if (user.data_exclusao || user.data_bloqueado) return cb();
            
            cb(null, new Usuario(payload));
        });
    }

    /**
     * Cria um novo usuário
     * @param {*} payload Dados do usuário que vem da API (google, facebook)
     * @param {*} cb Retorna Erro ou dados do usuário
     */
    function create(payload, cb) {
        const query = `
            INSERT INTO usuario (id_social, nome, email, img_url, data_inclusao)
                VALUES (?, ?, ?, ?, ?)
        `;

        const { sub, name, email, picture } = payload;
        
        mysql.executa(query, [ sub, name, email, picture, new Date() ], (err) => {
            if (err) return cb(err);

            payload.created = true;

            cb(null, new Usuario(payload));
        });
    }

    return {
        verifyAndCreate: verifyAndCreate,
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['mysql', 'models/Usuario'];