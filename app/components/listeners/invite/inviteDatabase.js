/**
 * Auth Database manager
 * Handle database connection for Authentication methods
 */

exports = module.exports = function(mysql, util, async) {

    /**
     * Receive all invitations available
     * @param {*} socialId 
     * @param {*} cb 
     */
    function getInvites(socialId, cb) {
        const query = `
            SELECT 
                u.id, u.id_social, u.nome, u.email, u.img_url, c.data_enviado, t.uuid
            FROM trajeto_convite c
                LEFT JOIN trajeto t ON t.uuid = c.trajeto_uuid
                LEFT JOIN usuario u ON u.id = t.usuario_id
                LEFT JOIN usuario me ON me.id = c.usuario_id
            WHERE 
                t.fim_trajeto_id IS NULL 
                AND me.id_social = ?
                AND c.data_aceito IS NULL
                AND c.data_recusado IS NULL
        `;

        mysql.executa(query, [socialId], (err, data) => {
            if (err) return cb(err);

            cb(null, data);
        });
    }

    /**
     * Receive all invitations available
     * @param {*} socialId 
     * @param {*} trajetoUuid
     * @param {*} cb 
     */
    function getInviteTrajeto(socialId, trajetoUuid, cb) {
        const query = `
            SELECT 
                u.id, u.id_social, u.nome, u.email, u.img_url, c.data_enviado, t.uuid
            FROM trajeto_convite c
                LEFT JOIN trajeto t ON t.uuid = c.trajeto_uuid
                LEFT JOIN usuario u ON u.id = t.usuario_id
                LEFT JOIN usuario me ON me.id = c.usuario_id
            WHERE 
                t.fim_trajeto_id IS NULL 
                AND me.id_social = ?
                AND t.uuid = ?
                AND c.data_aceito IS NULL
                AND c.data_recusado IS NULL
        `;

        mysql.executa(query, [socialId, trajetoUuid], (err, data) => {
            if (err) return cb(err);

            cb(null, data);
        });
    }

    /**
     * Send invites to route
     * @param {*} data 
     * @param {*} cb 
     */
    function createInvites(data, cb) {
        if (!data.convidados || !data.convidados.length) 
            return cb();

        const query = `
            INSERT INTO trajeto_convite (data_enviado, trajeto_uuid, usuario_id)
            VALUES (?, ?, 
                (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)
            )
        `;

        let asyncQuery = [];

        data.convidados.forEach((convidado) => 
            asyncQuery.push(cb => mysql.executa(query, [data.criacao, data.uuid, convidado.socialId], cb))
        );

        async.parallel(asyncQuery, cb);
    }

    /**
     * Respond invite
     * @param {*} data 
     * @param {*} cb 
     */
    function respond(data, cb) {
        if (!data.trajetoUuid || !data.socialId)
            return cb(true);

        const query = `
            UPDATE trajeto_convite SET
                data_aceito = ?,
                data_recusado = ?
            WHERE trajeto_uuid = ?
            AND usuario_id = 
                (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)
        `;

        mysql.executa(query, [
            data.data_aceito, data.data_recusado, data.trajetoUuid, data.socialId
        ], cb); 
    }

    return {
        createInvites,
        respond,

        getInvites,
        getInviteTrajeto,
    }
}

exports['@singleton'] = true;   
exports['@require'] = ['mysql', 'utilManager', 'async'];