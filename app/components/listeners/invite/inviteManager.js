/**
 * Route manager
 * Handle events for user route
 */

exports = module.exports = function(Database, Usuario, Convite) {

    function getInvites(socialId, cb) {
        Database.getInvites(socialId, (err, convites) => {
            if (err) return cb(err);
            if (!convites || !convites.length) return cb(true);

            convites = convites.map(convite => {
                const user = new Usuario(convite);

                return new Convite(convite.data_enviado, convite.uuid, user);
            });

            cb(null, convites);
        });
    }

    function getInviteTrajeto(socialId, trajetoId, cb) {
        Database.getInviteTrajeto(socialId, trajetoId, (err, convite) => {
            if (err) return cb(err);
            if (!convite) return cb(true);

            convite = convite[0];

            const user = new Usuario(convite);

            cb(null, new Convite(convite.data_enviado, convite.uuid, user));
        });
    }

    /**
     * Create invites for route
     * @param {*} data 
     * @param {*} cb 
     */
    function createInvites(data, cb) {
        Database.createInvites(data, cb);
    }

    /**
     * Respond invite
     * @param {*} data { socialId, trajetoUuid, aceito }
     * @param {*} cb 
     */
    function respond(data, cb) {
        let dateNow = new Date();

        if (data.aceito) {
            data.data_aceito = dateNow;
        } else {
            data.data_recusado = dateNow;
        }
        
        Database.respond(data, cb);
    }

    return {
        createInvites,
        respond,

        getInvites,
        getInviteTrajeto,
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['invite/inviteDatabase', 'models/Usuario', 'models/Convite'];