/**
 * Alert manager
 * Handle events for alert management
 */

exports = module.exports = function(Database, Alerta) {

    /**
     * Adiciona um novo alerta
     * @param {*} data 
     * @param {*} cb 
     */
    function add(data, cb) {
        data.dataCriacao = new Date(data.dataCriacao);

        Database.add(data, (err, data) => {
            if (err) return cb(err);

            const alerta = new Alerta(data);

            cb(null, alerta);
        });
    }

    /**
     * Recebe todos alertas
     * @param {*} data 
     * @param {*} cb 
     */
    function get(cb) {
        Database.get((err, data) => {
            if (err) return cb(err);
            
            let alertas = data.map(alert => new Alerta(alert));

            cb(null, alertas);
        });
    }

    return {
        get,
        add
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['alert/alertDatabase', 'models/Alerta'];