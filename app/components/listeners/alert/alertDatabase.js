/**
 * Alert manager manager
 * Handle database connection for Alert methods
 */

exports = module.exports = function(mysql, coordinatesManager, regionManager) {
    function add(data, cb) {
        const query = `
            INSERT INTO alerta 
                (descricao, data_criacao, tipo_alerta_id, coordenada_id, usuario_id)
            VALUES (?, ?, ?, ?,
                (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)
            )
        `;
        
        coordinatesManager.add(data.coordenada, (err, coordId) => {
            if (err) return cb(err);
            
            mysql.executa(query, [
                data.descricao, data.dataCriacao, data.alertaId, coordId, data.socialId
            ], (err, alerta) => {
                if (err) return cb(err);

                regionManager.getByRange(data.coordenada, (err, regionId) => {
                    if (err) return cb(err);

                    if (regionId) {
                        return regionManager.add(regionId, alerta.insertId, (err) => {
                            if (err) return cb(err);

                            getTipoAlerta(alerta.insertId, (err, tipo) => {
                                if (err) return cb(err);

                                getById(alerta.insertId, (err, alert) => {
                                    if (err) return cb(err);

                                    cb(null, alert);
                                });
                            });
                        });
                    }

                    regionManager.create(coordId, (err, id) => {
                        if (err) return cb(err);

                        regionManager.add(id, alerta.insertId, (err) => {
                            if (err) return cb(err);

                            getTipoAlerta(alerta.insertId, (err, tipo) => {
                                if (err) return cb(err);

                                getById(alerta.insertId, (err, alert) => {
                                    if (err) return cb(err);

                                    cb(null, alert);
                                });
                            });
                        });
                    });
                });
            });
        });
    }

    function get(cb) {
        const query = `
            SELECT 
                a.id as alertId, t.id as alertaId, t.nome as nome, a.descricao,
                DATE_ADD(a.data_criacao, INTERVAL t.expiracao MINUTE) as duracao,
                c.latitude, c.longitude, a.data_criacao 
            FROM alerta a
                LEFT JOIN tipo_alerta t ON t.id = a.tipo_alerta_id
                LEFT JOIN coordenada c ON c.id = a.coordenada_id
            WHERE DATE_ADD(a.data_criacao, INTERVAL t.expiracao MINUTE) < NOW()
        `;

        mysql.executa(query, [], cb);
    }

    function getById(id, cb) {
        const query = `
            SELECT 
                a.id as alertId, t.id as alertaId, t.nome as nome, a.descricao,
                DATE_ADD(a.data_criacao, INTERVAL t.expiracao MINUTE) as duracao,
                c.latitude, c.longitude, a.data_criacao 
            FROM alerta a
                LEFT JOIN tipo_alerta t ON t.id = a.tipo_alerta_id
                LEFT JOIN coordenada c ON c.id = a.coordenada_id
            WHERE 
                DATE_ADD(a.data_criacao, INTERVAL t.expiracao MINUTE) < NOW()
                AND a.id = ?
        `;

        mysql.executa(query, [id], (err, data) => {
            if (err) return cb(err);
            if (!data || !data[0]) return cb();

            cb(null, data[0]);
        });
    }

    function getTipoAlerta(alertId, cb) {
        const query = `
            SELECT a.data_criacao, t.expiracao, t.nome FROM alerta a
                LEFT JOIN tipo_alerta t ON a.tipo_alerta_id = t.id
            WHERE a.id = ?
        `;

        mysql.executa(query, [alertId], (err, data) => {
            if (err) return cb(err);
            if (!data || !data[0]) return cb();

            const duracao = new Date(
                data[0].data_criacao 
                && data[0].data_criacao.setMinutes(
                    data[0].data_criacao.getMinutes() + data[0].expiracao
                )
            );

            return cb(null, { nome: data[0].nome, duracao: duracao });
        });
    }

    return {
        get,
        add
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['mysql', 'coordinates/coordinatesManager', 'region/regionManager'];