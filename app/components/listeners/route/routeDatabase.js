/**
 * Auth Database manager
 * Handle database connection for Authentication methods
 */

exports = module.exports = function(mysql, util, inviteManager, coordinatesManager) {
    
    // 50 metros em km
    const RAIO_FINISH = .015;

    /**
     * Create a route for a specific user
     * @param { bateria, distancia, duracao, userId, partida, destino, tipoTrajeto } data
     */
    function create(data, cb) {
        const query = `
            INSERT INTO trajeto (
                uuid, data_inicio, bateria_inicio, bateria_baixa, distancia, duracao_estimada, 
                partida_coordenada_id, destino_coordenada_id, pontos, tipo_trajeto_id, usuario_id
            ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,
                (SELECT id FROM tipo_trajeto tt WHERE tt.modo LIKE UPPER(?) GROUP BY tt.modo),
                (SELECT id FROM usuario u WHERE u.id_social = ? GROUP BY id_social)
            )
        `;

        data.uuid = util.uuid();
        data.bateriaBaixa = data.bateria && data.bateria < 15;

        coordinatesManager.add(data.partida, (err, partidaId) => {
            if (err) return cb(err);

            coordinatesManager.add(data.destino, (err, destinoId) => {
                if (err) return cb(err);

                mysql.executa(
                    query, 
                    [data.uuid, data.criacao, data.bateria, data.bateriaBaixa, data.distancia, 
                        data.duracao, partidaId, destinoId, data.pontos, data.tipoTrajeto.modo, data.criador.socialId],
                    (err) => {
                        if (err) return cb(err);

                        inviteManager.createInvites(data, (err) => {
                            if (err) return cb(err);
                            
                            cb(null, data.uuid);
                        });
                    }
                )
            });
        });
    }

    /**
     * Finish the route
     * @param { bateria, data, tipoFimId, qtdAssistindo, routeUuid } data
     */
    function finish(data, cb) {
        const query = `
            INSERT INTO trajeto_fim (data, bateria, qtd_assistindo, tipo_fim_trajeto_id)
                VALUES (?, ?, ?, ?)
        `;

        const queryUpdateTrajeto = `
            UPDATE trajeto SET fim_trajeto_id = ?
            WHERE uuid = ?
        `;

        mysql.executa(query, [
            data.data, data.bateria, data.qtdAssistindo, data.tipoFimId
        ], (err, res) => {
            if (err) return cb(err);

            mysql.executa(queryUpdateTrajeto, [res.insertId, data.routeUuid], cb);
        });
    }

    /**
     * Disconnect user from the route
     * @param { data_desconectado, uuid, socialId } data
     */
    function disconnect(data, cb) {
        const query = `
            UPDATE trajeto_observador SET
                data_desconectado = ?
            WHERE 
                (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)
                AND trajeto_uuid = ?
        `;

        mysql.executa(query, [
            data.data_desconectado, data.socialId, data.uuid
        ], (err) => {
            if (err) return cb(err);

            cb();
        });
    }
    /**
     * Disconnect user from the route
     * @param { data_desconectado, uuid, socialId } data
     */
    function disconnectGuest(data, cb) {
        const query = `
            UPDATE trajeto_observador SET
                data_desconectado = ?
            WHERE 
                id = ?
                AND trajeto_uuid = ?
        `;

        mysql.executa(query, [
            data.data_desconectado, data.observerId, data.uuid
        ], (err) => {
            if (err) return cb(err);

            cb();
        });
    }

    /**
     * Create a new user who is observing
     * @param { latitude, longitude, local } data
     */
    function createObserver(data, cb) {
        const query = `
            INSERT INTO trajeto_observador 
                (trajeto_uuid, guest_ip, guest_agent, ativo, data_conectado, usuario_id)
            VALUES (?, ?, ?, ?, ?,
                (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)
            )
        `;

        verifyObserver(data, (err, obsId) => {
            if (err) return cb(err);

            if (obsId) return cb(null, obsId);

            mysql.executa(
                query, 
                [data.trajeto_uuid, data.guest_ip, data.guest_agent, 
                    data.ativo, data.data_conectado, data.socialId],
                (err, res) => {
                    if (err) return cb(err);
    
                    cb(null, res.insertId);
                });
        });
    }

    /**
     * Verify if the person is already an observer
     * @param {*} data 
     * @param {*} cb 
     */
    function verifyObserver(data, cb) {
        const query = `
            SELECT o.id FROM trajeto_observador o
                LEFT JOIN trajeto t ON t.uuid = o.trajeto_uuid
            WHERE 
                o.usuario_id = (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)
                AND o.trajeto_uuid = ?
                AND o.data_desconectado IS NULL
        `;

        mysql.executa(query, [data.socialId, data.trajeto_uuid],
            (err, res) => {
                if (err) return cb(err);
                if (!res || !res[0]) return cb();

                cb(null, res[0].id);
            });
    }

    /**
     * Select Person who requested the route - Owner
     * @param {*} uuid 
     */
    function getOwner(uuid, cb) {
        const query = `
            SELECT u.* FROM trajeto t
                LEFT JOIN usuario u ON u.id = t.usuario_id
            WHERE t.uuid = ?
        `;

        mysql.executa(query, [uuid], cb);
    }

    /**
     * Get route
     * @param {*} uuid 
     */
    function getRoute(uuid, cb) {
        const query = `
            SELECT uuid, data_inicio, bateria_inicio, avaliacao, distancia, duracao_estimada,
                pc.local as pLocal, pc.latitude as pLat, pc.longitude as pLong, 
                dc.local as dLocal, dc.latitude as dLat, dc.longitude as dLong,
                tt.descricao, tt.modo, t.usuario_id as userId, fim_trajeto_id as finalizado,
                t.pontos
            FROM trajeto t
                LEFT JOIN coordenada pc ON t.partida_coordenada_id = pc.id
                LEFT JOIN coordenada dc ON t.destino_coordenada_id = dc.id
                LEFT JOIN tipo_trajeto tt ON tt.id = t.tipo_trajeto_id
            WHERE uuid = ?
        `;

        mysql.executa(query, [uuid], (err, data) => {
            if (err || !data || !data.length) 
                return cb(err || true);

            return cb(null, data[0]);
        });
    }

    /**
     * Get every route someone is watching
     * @param {*} socialId 
     */
    function getWatching(socialId, cb) {
        const query = `
            SELECT uuid, data_inicio, bateria_inicio, avaliacao, distancia, duracao_estimada,
                pc.local as pLocal, pc.latitude as pLat, pc.longitude as pLong, 
                dc.local as dLocal, dc.latitude as dLat, dc.longitude as dLong,
                tt.descricao, tt.modo, t.usuario_id as userId, fim_trajeto_id as finalizado,
                u.id_social, u.nome, u.email, u.img_url, t.pontos
            FROM trajeto_observador too
                LEFT JOIN trajeto t ON too.trajeto_uuid = t.uuid
                LEFT JOIN coordenada pc ON t.partida_coordenada_id = pc.id
                LEFT JOIN coordenada dc ON t.destino_coordenada_id = dc.id
                LEFT JOIN tipo_trajeto tt ON tt.id = t.tipo_trajeto_id
                LEFT JOIN usuario u ON u.id = too.usuario_id
            WHERE 
                u.id_social = ?
                AND fim_trajeto_id IS NULL
                AND too.data_desconectado IS NULL
        `;

        mysql.executa(query, [socialId], cb);
    }

    function verifyFinish(uuid, coords, cb) {
        const query = `
            SELECT ( 6371 * 
                    ACOS( 
                        COS( RADIANS( f.latitude ) ) * 
                        COS( RADIANS( ? ) ) * 
                        COS( RADIANS( ? ) - 
                        RADIANS( f.longitude ) ) + 
                        SIN( RADIANS( f.latitude ) ) * 
                        SIN( RADIANS( ? ) ) 
                    ) 
                ) AS distance 
            FROM trajeto t 
                LEFT JOIN coordenada f ON f.id = t.destino_coordenada_id
            WHERE uuid = ? AND fim_trajeto_id IS NULL
            HAVING distance <= ${RAIO_FINISH} 
            ORDER BY distance ASC
        `;

        mysql.executa(query, [
            coords.latitude, coords.longitude, coords.latitude, uuid
        ], cb);
    }

    function getMyOpenRoute(socialId, cb) {
        const query = `
            SELECT uuid, data_inicio, bateria_inicio, avaliacao, distancia, duracao_estimada,
                pc.local as pLocal, pc.latitude as pLat, pc.longitude as pLong, 
                dc.local as dLocal, dc.latitude as dLat, dc.longitude as dLong,
                tt.descricao, tt.modo, t.usuario_id as userId, fim_trajeto_id as finalizado,
                t.pontos
            FROM trajeto t
                LEFT JOIN coordenada pc ON t.partida_coordenada_id = pc.id
                LEFT JOIN coordenada dc ON t.destino_coordenada_id = dc.id
                LEFT JOIN tipo_trajeto tt ON tt.id = t.tipo_trajeto_id
            WHERE 
                t.usuario_id = (SELECT id FROM usuario WHERE id_social = ?)
                AND fim_trajeto_id IS NULL
        `;

        mysql.executa(query, [socialId], (err, data) => {
            if (err || !data || !data.length) 
                return cb(err || true);

            return cb(null, data[0]);
        });
    }

    function userLeft(data, cb) {
        const query = `
            INSERT INTO trajeto_verificador (data_saida, trajeto_uuid, coordenada_id)
            VALUES (?, ?, ?)
        `;

        const queryVerify = `
            SELECT id FROM trajeto_verificador
            WHERE 
                trajeto_uuid = ?
                AND data_saida IS NOT NULL
                AND data_volta IS NULL
        `;

        mysql.executa(queryVerify, [data.trajetoUuid], (err, res) => {
            if (err) return cb(err);
            if (res && res[0]) return cb();

            coordinatesManager.add(data.coordenada, (err, coordsId) => {
                if (err) return cb(err);
    
                mysql.executa(query, [data.data, data.trajetoUuid, coordsId], cb);
            });
        });
    }

    function userBack(data, cb) {
        const query = `
            INSERT INTO trajeto_verificador (data_saida, data_volta, trajeto_uuid, coordenada_id)
            VALUES (?, ?, ?, ?)
        `;

        const queryVerify = `
            SELECT data_saida FROM trajeto_verificador
            WHERE 
                trajeto_uuid = ?
                AND data_saida IS NOT NULL
                AND data_volta IS NULL
        `;

        mysql.executa(queryVerify, [data.trajetoUuid], (err, res) => {
            if (err) return cb(err);
            if (!res || !res[0]) return cb();

            coordinatesManager.add(data.coordenada, (err, coordsId) => {
                if (err) return cb(err);
    
                mysql.executa(query, [res[0].data_saida, data.data, data.trajetoUuid, coordsId], cb);
            });
        });
    }

    return {
        create: create,
        finish: finish,

        disconnect: disconnect,
        disconnectGuest: disconnectGuest,

        createObserver: createObserver,

        getOwner: getOwner,

        getRoute: getRoute,
        getWatching: getWatching,

        verifyFinish: verifyFinish,
        getMyOpenRoute: getMyOpenRoute,

        userLeft: userLeft,
        userBack: userBack
    }
}

exports['@singleton'] = true;   
exports['@require'] = [
    'mysql', 'utilManager', 'invite/inviteManager', 'coordinates/coordinatesManager'
];