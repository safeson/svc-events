/**
 * Route manager
 * Handle events for user route
 */

exports = module.exports = function(Database, Trajeto, Usuario, TipoManager) {

    /**
     * Connect to a specific route
     * @param { Websocket obj } ws
     */
    function connect(ws, cb) {
        getRoute(ws.key, (err, route) => {
            if (err) return cb(err);

            if (route.finalizado) {
                return cb("Rota está finalizada");
            }

            ws.route = route;

            const isGuest = !ws.socialId;
    
            const obj = {
                trajeto_uuid: ws.key,
                socialId: ws.socialId,
            };
    
            if (isGuest) {
                obj.guest_ip = ws.ip;
                obj.guest_agent = ws.agent;
            }
    
            getOwner(ws, (err, owner) => {
                if (err) return cb(err);

                ws.route.criador = owner;

                if (owner.myself) return cb();

                ws.clients.forEach((cli) => {
                    const found = cli.socialId == owner.socialId;
                    
                    if (found) {
                        ws.owner = owner;
                    }
                });
    
                createObserver(obj, (err, obsId) => {
                    if (err) return cb(err);
    
                    ws.observer = obsId;
    
                    cb();
                });
            });
        });
    }

    /**
     * Disconnect user from a specific route
     * @param { Websocket obj } ws
     */
    function disconnect(uuid, socialId, cb) {
        const params = {
            data_desconectado: new Date(),
            uuid: uuid,
            socialId: socialId
        };

        Database.disconnect(params, cb);
    }

     /**
     * Disconnect user from a specific route
     * @param { Websocket obj } ws
     */
    function disconnectGuest(uuid, observerId, cb) {
        const params = {
            data_desconectado: new Date(),
            uuid: uuid,
            observerId: observerId
        };

        Database.disconnectGuest(params, cb);
    }

    /**
     * Cria um observador para uma rota especifica
     * @param {*} data trajeto_uuid e socialId
     * @param {*} cb 
     */
    function createObserver(data, cb) {
        data.data_conectado = new Date();
        data.ativo = true;

        Database.createObserver(data, cb);
    }

    /**
     * Person who requested the route - Owner
     * @param {*} uuid 
     */
    function getOwner(ws, cb) {
        Database.getOwner(ws.key, (err, res) => {
            if (err || !res || !res[0]) return cb(err);

            let owner = new Usuario(res[0]);

            owner.myself = ws.socialId == owner.socialId;

            cb(null, owner);
        });
    }
   
    /**
     * Start route
     * @param { bateria, distancia, duracao, userId, partida, destino, tipoTrajeto } data
     */
    function start(data, cb) {
        data.criacao = new Date();
        data.pontos = JSON.stringify(data.pontos);

        data.convidados = Object.values(
            data.convidados.reduce((acc, cur) => Object.assign(acc, { [cur.socialId]: cur }), {})
        );
        
        Database.create(data, (err, uuid) => {
            if (err) return cb(err);

            data.uuid = uuid;

            cb(null, new Trajeto(data), data.convidados);
        });
    }

    /**
     * Finish route
     * @param { bateria, tipoFim, routeUuid, qtdAssistindo } data
     */
    function finish(data, cb) {
        data.data = new Date();
        data.tipoFimId = TipoManager.fimTrajeto(data.tipoFim);
        
        Database.finish(data, cb);
    }

    /**
     * Get every route someones watching
     * @param { socialId } 
     */
    function getWatching(socialId, cb) {        
        Database.getWatching(socialId, (err, data) => {
            if (err) return cb(err);

            data = data.map((item) => {
                item.criador = {
                    id: item.userId,
                    nome: item.nome,
                    email: item.email,
                    img_url: item.img_url,
                    id_social: item.id_social,
                };
                return new Trajeto(item);
            });

            return cb(null, data);
        });
    }

    /**
     * Verify if route exists or not
     * @param {*} uuid 
     * @param {*} cb If not, return error
     */
    function getRoute(uuid, cb) {
        Database.getRoute(uuid, (err, data) => {
            if (err || !data) return cb(err || "Rota inexistente");

            cb(null, new Trajeto(data));
        });
    }

    /**
     * Get open route
     * @param {*} uuid 
     * @param {*} cb If not, return error
     */
    function getMyOpenRoute(socialId, cb) {
        Database.getMyOpenRoute(socialId, (err, data) => {
            if (err || !data) return cb(err || "Rota inexistente");

            cb(null, new Trajeto(data));
        });
    }

    /**
     * Verify if route has finished
     * @param {*} uuid 
     * @param {*} coords 
     * @param {*} cb 
     */
    function verifyFinish(uuid, coords, cb) {
        Database.verifyFinish(uuid, coords, (err, data) => {
            if (err) return cb(err);

            return cb(null, data && data[0]);
        });
    }

    /**
     * Set trajeto verificador left
     * @param {} data 
     */
    function userLeft(data, cb) {
        Database.userLeft(data, cb);
    }

    /**
     * Set trajeto verificador back on track
     * @param {} data 
     */
    function userBack(data, cb) {
        Database.userBack(data, cb);
    }

    return {
        start: start,
        finish: finish,

        connect: connect,
        disconnect: disconnect,
        disconnectGuest: disconnectGuest,

        createObserver: createObserver,

        getRoute: getRoute,
        getWatching: getWatching,

        verifyFinish: verifyFinish,
        getMyOpenRoute: getMyOpenRoute,

        userLeft: userLeft,
        userBack: userBack
    };
}

exports['@singleton'] = true;   
exports['@require'] = [
    'route/routeDatabase', 'models/Trajeto', 'models/Usuario', 'tipoManager'
];