exports = module.exports = function(Database, Usuario) {

    /**
     * Recebe os amigos a partir de um id ou a lista de todos
     */
    function get(socialId, friendId, cb) {
        if (friendId)
            return getFriendById(socialId, friendId, cb);

        listFriends(socialId, cb);
    }

    /**
     * Lista todos
     */
    function getFriendById(socialId, friendId, cb) {
        Database.getFriendById(socialId, friendId, (err, friend) => {
            if (err) return cb(err);

            cb(null, new Usuario(friend));
        });
    }

    /**
     * Lista todos amigos de um usuario
     */
    function listFriends(socialId, cb) {
        Database.listFriends(socialId, (err, friends) => {
            if (err) return cb(err);

            cb(null, friends.map(fr => new Usuario(fr)));
        });
    }

    /**
     * Adiciona amigo por e-mail
     */
    function add(socialId, email, cb) {
        Database.add(socialId, email, cb);
    }

    /**
     * Deleta amigo
     */
    function remove(socialId, friendId, cb) {
        Database.remove(socialId, friendId, cb);
    }

    return {
        get,
        add,
        remove
    };

}

exports['@singleton'] = true;   
exports['@require'] = ['friends/friendsDatabase', 'models/Usuario'];