exports = module.exports = function(mysql, Usuario, async) {

    /**
     * Recebe um amigo pelo id especifico
     */
    function getFriendById(friendId, cb) {
        const query = `
            SELECT f.id, f.id_social, f.nome, f.email, f.img_url, f.data_inclusao FROM usuario f
            WHERE f.id = ?
        `;

        mysql.executa(query, [friendId], (err, friend) => {
            if (err) return cb(err);
            if (!friend[0]) return cb(true);

            cb(null, new Usuario(friend[0]));
        });
    }

     /**
     * Remove amigo
     */
    function remove(userId, friendId, cb) {
        const query = `
            DELETE FROM contato 
            WHERE 
                usuario_id = (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)
                AND contato_id = (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)
        `;

        mysql.executa(query, [userId, friendId], (err) => {
            if (err) return cb(err);

            cb();
        });
    }

    /**

    /**
     * Recebe um amigo pelo email especifico
     */
    function getFriendByEmail(email, cb) {
        const query = `
            SELECT f.id, f.id_social, f.nome, f.email, f.img_url, f.data_inclusao FROM usuario f
            WHERE f.email = ?
        `;

        mysql.executa(query, [email], (err, friend) => {
            if (err) return cb(err);
            if (!friend[0]) return cb(true);

            cb(null, new Usuario(friend[0]));
        });
    }

    /**
     * Recebe todos amigos de um usuario
     */
    function listFriends(socialId, cb) {
        const query = `
            SELECT f.id, f.id_social, f.nome, f.email, f.img_url, f.data_inclusao FROM contato c
                LEFT JOIN usuario u ON u.id = c.usuario_id
                LEFT JOIN usuario f ON f.id = c.contato_id
            WHERE u.id_social = ?
        `;

        mysql.executa(query, [socialId], (err, friends) => {
            if (err) return cb(err);
            if (!friends || !friends.length) return cb(true);

            cb(null, friends);
        });
    }

    /**
     * Adiciona um amigo para o usuario por email
     * @param {*} socialId 
     * @param {*} email 
     * @param {*} cb 
     * @returns Usuario adicionado
     */
    function add(socialId, email, cb) {
        const query = `
            INSERT INTO contato (usuario_id, contato_id) 
            VALUES (
                (SELECT id FROM usuario u WHERE u.id_social = ? GROUP BY id_social),
                (SELECT id FROM usuario a WHERE a.email = ?)
            )
        `;
        
        let asyncArr = [];
        let user = {};

        asyncArr.push((cb) => mysql.executa(query, [socialId, email], cb));
        asyncArr.push((cb) => getFriendByEmail(email, (err, res) => {
            if (err) return cb(err);

            user = res;
            cb();
        }));

        async.parallel(asyncArr, (err) => {
            if (err) return cb(err);

            cb(null, user);
        });
    }

    return {
        getFriendById,
        listFriends,

        add,
        remove
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['mysql', 'models/Usuario', 'async'];