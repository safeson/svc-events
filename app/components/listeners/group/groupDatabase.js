exports = module.exports = function(mysql, Usuario, async) {

    function add(grupo, cb) {
        const query = `
            INSERT INTO grupo (nome, usuario_id)
            VALUES (?, (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social))
        `;

        const queryMembros = `
            INSERT INTO usuario_grupo (usuario_id, grupo_id)
            VALUES ((SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social), ?)
        `;

        mysql.executa(query, [grupo.nome, grupo.socialId], (err, res) => {
            if (err) return cb(err);

            let asyncArr = [];

            grupo.id = res.insertId;

            grupo.membros.forEach(socialId => {
                asyncArr.push((cb) => {
                    mysql.executa(queryMembros, [socialId, grupo.id], cb);
                });
            });

            async.parallel(asyncArr, (err) => {
                cb(err, grupo);
            });
        });
    }

    function get(socialId, cb) {
        const query = `
            SELECT g.nome, g.id, u.id_social as socialId
            FROM grupo g
                LEFT JOIN usuario u ON u.id = g.usuario_id
            WHERE u.id_social = ?
            ORDER BY g.id
        `;

        const querySelectUsuarioGrupo = `
            SELECT u.id_social FROM usuario_grupo ug
                LEFT JOIN usuario u ON u.id = ug.usuario_id
            WHERE ug.grupo_id = ?
        `;

        mysql.executa(query, [socialId], (err, grupos) => {
            if (err) return cb(err);

            let asyncArr = [];

            grupos.map(gr => {
                asyncArr.push(
                    (cb) => mysql.executa(querySelectUsuarioGrupo, [gr.id], (err, membros) => {
                        if (err) return cb(err);

                        gr.membros = membros.map(m => m.id_social);

                        cb();
                    })
                );
            });

            async.parallel(asyncArr, (err) => {
                if (err) return cb(err);

                cb(null, grupos);
            });
        });
    }

    function remove(id, cb) {
        const query = `
            DELETE FROM usuario_grupo WHERE grupo_id = ?
        `;

        const queryGrupo = `
            DELETE FROM grupo WHERE id = ?
        `;

        mysql.executa(query, [id], (err) => {
            if (err) return cb(err);

            mysql.executa(queryGrupo, [id], (err) => {
                if (err) return cb(err);
    
                return cb();
            });
        });
    }
    
    return {
        add,
        get,
        remove
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['mysql', 'models/Usuario', 'async'];