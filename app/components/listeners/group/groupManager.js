exports = module.exports = function(Database, Grupo) {

    function add(data, cb) {
        const params = {
            membros: data.membros,
            nome: data.nome,
            socialId: data.socialId
        };

        Database.add(params, (err, grupo) => {
            if (err) return cb(err);

            cb(null, new Grupo(grupo));
        });
    }

    function get(socialId, cb) {
        Database.get(socialId, (err, grupos) => {
            if (err) return cb(err);

            cb(null, grupos.map(gr => {
                if (!gr.id) return null;
                return new Grupo(gr);
            }).filter(g => g));
        });
    }

    function remove(id, cb) {
        Database.remove(id, cb);
    }

    return {
        add,
        get,
        remove
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['group/groupDatabase', 'models/Grupo'];