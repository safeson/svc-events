 /**
 * Region manager
 * Handle events for region management
 */

exports = module.exports = function(Database) {
    function add(data, id, cb) {
        Database.add(data, id, cb);
    }

    function create(data, cb) {
        Database.create(data, cb);
    }

    function getByRange(data, cb) {
        Database.getByRange(data, cb);
    }

    return {
        add,
        create,

        getByRange
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['region/regionDatabase'];