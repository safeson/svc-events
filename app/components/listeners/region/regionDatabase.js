exports = module.exports = function(mysql) {

    // IN KM
    const REGION_RADIUS = 5;

    /**
     * Get a danger region by coordinate
     * @param {*} coords
     * @param {*} cb 
     */
    function getByRange(coords, cb) {
        const query = `
            SELECT r.id,
                 ( 6371 * 
                    ACOS( 
                        COS( RADIANS( c.latitude ) ) * 
                        COS( RADIANS( ? ) ) * 
                        COS( RADIANS( ? ) - 
                        RADIANS( c.longitude ) ) + 
                        SIN( RADIANS( c.latitude ) ) * 
                        SIN( RADIANS( ? ) ) 
                    ) 
                ) AS distance 
            FROM regiao_perigo r 
                LEFT JOIN coordenada c ON c.id = r.coordenada_id
            HAVING distance <= ${REGION_RADIUS} 
            ORDER BY distance ASC
        `;

        mysql.executa(query, [
            coords.latitude, coords.longitude, coords.latitude
        ], (err, data) => {
            if (err) return cb(err);

            return cb(null, data.length && data[0].id);
        });
    }

    /**
     * Add point to danger region 
     */
    function add(regionId, alertId, cb) {
        const query = `
            INSERT INTO regiao_perigo_alerta (regiao_perigo_id, alerta_id)
                VALUES (?, ?)
        `;

        mysql.executa(query, [regionId, alertId], (err, data) => {
            if (err) return cb(err);

            return cb(null, data && data.insertId);
        });
    }

    /**
     * Create danger region based on a center alert point
     * @param {*} pointId 
     * @param {*} cb 
     */
    function create(pointId, cb) {
        const query = `
            INSERT INTO regiao_perigo (raio, coordenada_id, localidade)
                VALUES (?, ${pointId},   
                    (SELECT local FROM coordenada WHERE id = ${pointId})
                )
        `;

        mysql.executa(query, [REGION_RADIUS], (err, data) => {
            if (err) return cb(err);

            return cb(null, data && data.insertId);
        });
    }

    return {
        add,
        create,

        getByRange
    };
}

exports['@singleton'] = true;   
exports['@require'] = ['mysql'];