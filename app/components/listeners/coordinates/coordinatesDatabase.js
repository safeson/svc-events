
exports = module.exports = function(mysql) {
    /**
     * Create a new coord
     * @param { latitude, longitude, local } data
     */
    function add(data, cb) {
        const query = `
            INSERT INTO coordenada (latitude, longitude, local) VALUES (?, ?, ?)
        `

        mysql.executa(query, [data.latitude, data.longitude, data.local], (err, data) => {
            if (err) return cb(err);

            return cb(null, data.insertId);
        });
    }

    return {
        add
    }
}

exports['@singleton'] = true;   
exports['@require'] = ['mysql'];