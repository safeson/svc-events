
exports = module.exports = function(Database) {
    /**
     * Create a new coord
     * @param { latitude, longitude, local } data
     */
    function add(data, cb) {
        Database.add(data, cb);
    }

    return {
        add
    }
}

exports['@singleton'] = true;   
exports['@require'] = ['coordinates/coordinatesDatabase'];