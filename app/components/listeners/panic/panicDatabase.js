
exports = module.exports = function(mysql, coordinatesManager) {
    /**
     * Create a new panic
     * @param {  } data
     */
    function add(data, cb) {
        const query = `
            INSERT INTO panico (data, trajeto_uuid, coordenada_id, usuario_id)
            VALUES (?, ?, ?, 
                (SELECT id FROM usuario WHERE id_social = ? GROUP BY id_social)    
            )
        `;

        coordinatesManager.add(data.coordenada, (err, coordId) => {
            if (err) return cb(err);

            mysql.executa(query, [
                data.data, data.trajetoUuid, coordId, data.socialId
            ], (err, panic) => {
                if (err) return cb(err);

                cb(null, panic && panic.insertId);
            });
        });

    }

    return {
        add
    }
}

exports['@singleton'] = true;   
exports['@require'] = ['mysql', 'coordinates/coordinatesManager'];