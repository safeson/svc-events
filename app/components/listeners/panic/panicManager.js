
exports = module.exports = function(Database, Panico) {
    /**
     * Create a new panic
     * @param { trajeto } data
     */
    function add(data, cb) {
        data.data = new Date();

        Database.add(data, (err, id) => {
            if (err) return cb(err);

            data.id = id;

            cb(null, new Panico(data));
        });
    }

    return {
        add
    }
}

exports['@singleton'] = true;   
exports['@require'] = ['panic/panicDatabase', 'models/Panico'];