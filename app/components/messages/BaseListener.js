exports = module.exports = function(resultManager, WebSocket) {

    /**
     * Base Listener constructor
     * @param {*} receive Receive data function
     * @param {*} send Data that should be returned function
     */
    function BaseListener(execute) {
        this.executeListener = execute;
        
        this.execute = this.executeWrapper;
    }

    /**
     * Put params sent to listener in the Object scope
     * @param {*} ws - Websocket Server Obj
     * @param {*} data - Data Sent by client
     */
    BaseListener.prototype.executeWrapper = function(ws, message) {
        this.ws = ws;
        this.data = message.data;
        this.token = message.token;

        this.executeListener();
    }

    /**
     * Send success message
     */
    BaseListener.prototype.success = function(data) {
        // Tratamento para array
        if (data && data.length) {
            data = { res: data };
        }

        const success = resultManager.success(data, this.token, this.ws.key);
        
        console.log(`[${this.token}] Success sent -> ${success}`);
        this.ws.send(success);
    }

    /**
     * Send error message
     */
    BaseListener.prototype.error = function(msg) {
        const error = resultManager.error(msg, this.token, this.ws.key);
        
        console.log(`[${this.token}] Error sent -> ${error}`);
        this.ws.send(error);
    }

    /**
     * Send missing params error message
     */
    BaseListener.prototype.missingParams = function() {
        this.ws.send(resultManager.error(resultManager.CODE.NO_PARAMS));
    }

    /**
     * Send the response to everyone but the client who sent this message
     * @param {*} data 
     */
    BaseListener.prototype.broadcast = function(data, name) {
        let that = this;
        const dataToBroadcast = resultManager.broadcast(data, name, this.ws.key);
        
        console.log(`[${this.token}] Data Broadcasted -> ${dataToBroadcast}
            Listeners -> ${this.ws.clients.size - 1}`);
            
        this.ws.clients.forEach((client) => {
            // if (client === that.ws) return;
            if (that.ws.key !== client.key) return;
            if (client.readyState !== WebSocket.OPEN) return;

            client.send(dataToBroadcast);
        });
    }

    /**
     * Send the response to a specific client on the same connection
     * @param {*} socialId
     * @param {*} data 
     */
    BaseListener.prototype.broadcastTo = function(socialId, data, name) {
        let that = this;
        const dataToBroadcast = resultManager.broadcast(data, name, this.ws.key);
        
        console.log(`[${this.token}] Data Broadcasted -> ${dataToBroadcast}
            To -> ${socialId}`);
            
        this.ws.clients.forEach((client) => {
            if (that.ws.key === client.key 
                && client.socialId === socialId
                && client.readyState === WebSocket.OPEN) {
                
                client.send(dataToBroadcast);
            }
        });

    }
 
    return BaseListener;
}

exports['@singleton'] = false;
exports['@require'] = ['resultManager', 'ws'];