/**
 * Verify every message received from socket
 */
exports = module.exports = function(listenerFiles, async, resultManager) {

    /**
     * Configure which file is going to listen to each action
     * @param {*} ws Websocket object
     */
    function configureListeners(ws, routeOnly, cb) {
        let listenerFunctions = {};

        let files = []
        listenerFiles.list.forEach((action) => {
            if (routeOnly && action.indexOf('route') === -1) {
                return;
            }
            
            files.push((cb) => 
                listenerFiles.create(action)
                    .then((actionFile) => {
                        listenerFunctions[action] = (params) => {                        
                            return actionFile.execute(ws, params);
                        }
                        cb();
                    })
                    .catch(err => {
                        console.error(err);
                    })
            );
        }, this);

        async.parallel(files, () => {
            cb(listenerFunctions);
        });
    }

    /**
     * Configurate every file in listeners folder to receive his own message
     */
    return function(ws, routeOnly) {
        configureListeners(ws, routeOnly, (listenerFunctions) => {
            ws.on('message', (data) => {
                let message = null;
                try {
                    message = JSON.parse(data);
                } catch (err) {
                    return ws.send(resultManager.error(resultManager.CODE.INVALID));
                }
    
                if (!message.action) 
                    return ws.send(resultManager.error(resultManager.CODE.NO_ACTION)); 
    
                if (!message.data) 
                    return ws.send(resultManager.error(resultManager.CODE.NO_PARAMS)); 
    
                console.log(
                    '[' + ws.token + ']' +
                    ' Message Received:' +
                    ' Horario -> ' + message.time +
                    ' Dados -> ' + JSON.stringify(message.data)
                );
    
                try {
                    const actionKey = Object.keys(listenerFunctions)
                        .find((key) => key.indexOf(message.action) > -1);
                    listenerFunctions[actionKey](message);
                } catch (err) {
                    console.error('Retorno invalido -> ' + err);
                    return ws.send(
                        resultManager.error(resultManager.CODE.INVALID_ACTION, message ? message.token : null)
                    ); 
                }
            });
        });
    }
}

exports['@singleton'] = true;
exports['@require'] = ['listenerFiles', 'async', 'resultManager'];