exports = module.exports = function(IoC, fs, path) {
	var basePath = process.cwd();
	var listeners = [];
	
	process.chdir(basePath + '/app/listeners');

	// List all files in a directory in Node.js recursively in a synchronous fashion
	var walkSync = (dir, filelist) => {
		var	files = fs.readdirSync(dir);
		filelist = filelist || [];
		files.forEach((file) => {
			if (fs.statSync(path.join(dir, file)).isDirectory()) {
				filelist = walkSync(path.join(dir, file), filelist);
			} else {
				filelist.push(path.join(dir, file));
			}
		});
		return filelist;
	};

	var listFiles = [];
	listeners = walkSync('.', listFiles)
		.filter((file) => {
			return file.endsWith('.js') && !file.endsWith('.spec.js');
		})
		.map((file) => {
			return file.split('.js')[0].replace(/[\\]/g, '/');
		});

	process.chdir(basePath);

	return {
		list: listeners,

		create: (name) => {
			var listenerPromise = IoC.create(name);
			return listenerPromise;
		}
	};
};

exports['@singleton'] = true;
exports['@require'] = ['electrolyte', 'fs', 'path'];
