/**
 * Error messages and types
 */
exports = module.exports = function() {
    const MESSAGES = {
        INVALID: 'Invalid message',
        NO_ACTION: 'Missing action',
        NO_PARAMS: 'Missing parameters',
        INVALID_ACTION: 'Invalid action'
    };

    /**
     * @returns { JSON } Default error obj
     */
    function error(msg, token, key) {
        let base = {
            error: true,
            msg: msg,
            key: key
        };

        if (token) base.token = token;

        console.error(msg);
        return JSON.stringify(base);
    }

    /**
     * @returns { JSON } Default success obj
     */
    function success(obj, token, key) {
        let base = {
            error: false,
            token: token,
            key: key
        };

        if (obj) base.result = obj;

        try {
            return JSON.stringify(base);
        } catch (err) {
            return error(err);
        }
    }

    /**
     * @returns { JSON } Default broadcast obj
     */
    function broadcast(obj, name, key) {
        let base = {
            error: false,
            broadcast: true,
            event: name,
            key: key
        };

        if (obj) base.result = obj;

        try {
            return JSON.stringify(base);
        } catch (err) {
            return error(err);
        }
    }

    return {
        CODE: MESSAGES,

        error: error,
        success: success,
        broadcast: broadcast
    }
}

exports['@singleton'] = true;
exports['@require'] = [];