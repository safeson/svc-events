/**
 * Util Manager
 */

exports = module.exports = function() {

    const FIM_TRAJETO = {
        CONCLUIDO: 1,
        CANCELADO: 2,
        CONEXAO_PERDIDA: 3,
        INTERROMPIDO: 4
    };

    /**
     * Get the id of referred description
     */
    function fimTrajeto(desc) {
        return FIM_TRAJETO[desc] || FIM_TRAJETO.CONCLUIDO;
    }

    return {
        fimTrajeto: fimTrajeto,
    }
}

exports['@singleton'] = true;   
exports['@require'] = [];