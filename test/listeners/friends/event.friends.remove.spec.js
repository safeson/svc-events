let expect = require('expect.js');

const dados = {
    "action": "event.friends.remove",
    "data": {
        "socialId": 106477995095735420884,
        "friendId": 1010
    }
};

const returnData = {
    "error": false
}

describe('event.friends.remove', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.friends.remove'));
});
