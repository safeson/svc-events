let expect = require('expect.js');

// pode ser só com friendId e só com socialId

const dados = {
    "action": "event.friends.get",
    "data": {
        "socialId": 106477995095735420884,
        "friendId": null
    }
};

const returnData = {
    "error": false,
    "result": {
        "res": [{
            "id": 7,
            "nome": "outro contato",
            "email": "ee@e.com",
            "imgUrl": null,
            "socialId": "1010",
            "created": false
        }, {
            "id": 8,
            "nome": "another one",
            "email": "a@a.a",
            "imgUrl": null,
            "socialId": null,
            "created": false
        }]
    }
};

describe('event.friends.get', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.friends.get'));
});
