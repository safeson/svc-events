let expect = require('expect.js');

// pode ser só com friendId e só com socialId

const dados = {
    "action": "event.friends.add",
    "token": 541212,
    "data": {
        "socialId": 106477995095735420884,
        "email": "email@email.com"
    }
};

const returnData = {
    "error": false,
    "token": 541212,
    "result": {
        "id": 8,
        "nome": "Amigo adicionado",
        "email": "email@email.com",
        "imgUrl": null,
        "socialId": null,
        "created": false
    }
};

describe('event.friends.add', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.friends.add'));
});
