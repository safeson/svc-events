let expect = require('expect.js');

/**
 * Alertas:
 * id, nome, gravidade, duracao (minutos)
 *  (1, 'Assalto', 4, 60), 
 *  (2, 'Furto', 1, 20), 
 *  (3, 'Assédio sexual', 3, 30), 
 *  (4, 'Sequestro', 5, 120),
 *  (5, 'Animal Perigoso', 2, 30),
 *  (6, 'Pouca iluminação', 1, 20);
*/

const dados = {
    "action": "event.alert.add",
    "token": 21421,
    "data": {
        "alertaId": 1, 
        "descricao": "Mano chegaram os cara e mataram a mina do meu lado",
        "coordenada": {
            "latitude": 241.42,
            "longitude": 41.1
        },
        "socialId": 219829084
    }
};

// Retorna um broadcast do alerta
const returnData = {
    "error": false,
    "broadcast": true,
    "event": "alert.add",
    "result": {
        "id": 10,
        "duracao": "2018-11-10T14:53:38.000Z",
        "coordenada": {
            "latitude": 24.42,
            "longitude": 41.1
        },
        "data_criacao": "2018-11-10T14:13:38.000Z",
        "descricao": "Mano chegaram os cara e mataram a mina do meu lado",
        "nome": "Assalto",
        "tipoId": 1,
    }
}

describe('event.alert.add', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.alert.add'));
});
