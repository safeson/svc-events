let expect = require('expect.js');

const dados = {
    "action": "event.alert.get",
    "token": 421421,
    "data": {
    }
};

// Retorna um array de alertas
const returnData = {
    "error": false,
    "token": 42142112,
    "result": {
      "res": [{
        "id": 1,
        "duracao": "2018-11-10T14:46:29.000Z",
        "coordenada": {
          "latitude": 24.42,
          "longitude": 41.1,
          "local": null
        },
        "data_criacao": "2018-11-10T14:13:38.000Z",
        "descricao": "Mano chegaram os cara e mataram a mina do meu lado",
        "nome": "Assalto",
        "tipoId": 1
      }, {
        "id": 2,
        "duracao": "2018-11-10T14:46:54.000Z",
        "coordenada": {
          "latitude": 24.42,
          "longitude": 41.1,
          "local": null
        },
        "data_criacao": "2018-11-10T14:13:38.000Z",
        "descricao": "Mano chegaram os cara e mataram a mina do meu lado",
        "nome": "Assalto",
        "tipoId": 1
      }]
    }
}

describe('event.alert.get', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.alert.get'));
});
