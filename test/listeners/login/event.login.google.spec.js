let expect = require('expect.js');

const dados = {
    "action": "event.login.google",
    "token": "UUID_REQUISICAO",
    "data": {
        "idToken": "TOKEN_ID_GOOGLE"
    }
};

const returnData = {
    "error": false,
    "token": "UUID_REQUISICAO",
    "result": {
        // new Usuario(....)
        "nome": "NOME TESTE",
        "email": "teste@teste.com",
        "imgUrl": "https://endereco.com/imgpng.png",
        "socialId": 412421421,
        "created": false
    }
};

describe('event.login.google', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.login.google'));

    it('should have idToken', () => expect(dados['data'].idToken).to.be.true);
    it('should have valid token reference', () => expect(dados.token).to.be.equal(returnData.token));
});
