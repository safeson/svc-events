let expect = require('expect.js');

const dados = {
    "action": "event.group.update",
    "token": 21421,
    "data": {
        "id": 10,
        "nome": "Grupo Teste", 
        "socialId": "112317189667567994320",
        "membros": ["1010", "106477995095735420884"]
    }
};

// Retorna um broadcast do alerta
const returnData = {
    "error": false,
    "result": {
        "id": 10,
        "nome": "Grupo Teste", 
        "socialId": "112317189667567994320",
        "membros": ["1010", "106477995095735420884"]
    }
}

describe('event.group.update', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.group.update'));
});
