let expect = require('expect.js');

const dados = {
    "action": "event.group.remove",
    "data": {
        "id": 1
    }
};

// Retorna um broadcast do alerta
const returnData = {
    "error": false,
    "result": {
    }
}

describe('event.group.remove', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.group.remove'));
});
