let expect = require('expect.js');

const dados = {
  "action": "event.group.get",
  "data": {
    "socialId": "106477995095735420884"
  }
};

// Retorna um array de alertas
const returnData = {
    "error": false,
    "token": 42142112,
    "result": {
      "res": [{
        "id": 8,
        "membros": ["1010", "112317189667567994320"],
        "nome": "Grupo test",
        "socialId": "106477995095735420884"
      }]
    }
}

describe('event.group.get', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.group.get'));
});
