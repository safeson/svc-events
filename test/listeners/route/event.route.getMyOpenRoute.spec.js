let expect = require('expect.js');

const dados = {
    "action": "event.route.getMyOpenRoute",
    "token": "a92b9802-7790-451f-889b-2d1b71bc9f62",
    "data": {
        "socialId": 412421421
    }
};

const returnData = {
    "error": false,
    "token": "a92b9802-7790-451f-889b-2d1b71bc9f62",
    "result": {
        "dataInicio": "2018-11-10T15:30:54.000Z",
        "uuid": "8cf7e692-5d74-455e-a1d8-ce06940ae696",
        "bateria": 22,
        "avaliacao": null,
        "duracao": 1000,
        "userId": 6,
        "finalizado": false,
        "pontos": [{
            "latitude": 42.11,
            "longidute": 11.22
        }, {
            "latitude": 2.11,
            "longidute": 1.22
        }, {
            "latitude": 4.11,
            "longidute": 11.22
        }, {
            "latitude": 1,
            "longidute": 2
        }],
        "tipoTrajeto": {
            "descricao": "Andando",
            "modo": "WALKING"
        },
        "partida": {
            "latitude": 48.858093,
            "longitude": 2.294694,
            "local": "The Eiffel Tower, Paris, France"
        },
        "destino": {
            "latitude": 41.890251,
            "longitude": 12.492373,
            "local": "Map of The Colosseum of Rome, Rome, Italy"
        },
        "criador": {
            "id": 6,
            "nome": "outro contato",
            "email": "ee@e.com",
            "imgUrl": null,
            "socialId": "1010",
            "created": false
        }
    }
}

describe('event.route.getWatching', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.route.getWatching'));

    it('should have valid token reference', () => expect(dados.token).to.be.equal(returnData.token));

    it('should have valid array of objects', () => expect(returnData['result'].res).to.not.be.empty);
});
