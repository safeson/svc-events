let expect = require('expect.js');

const dados = {
    "action": "event.invite.respond",
    "token": "TOKEN_ID_REFERENCE",
    "data": {
        "socialId": 106477995095735420884,
        "trajetoUuid": "09f02f1b-3a12-4227-9374-988feafb41a2",
        "aceito": true
    }
};

const returnData = {
    "error": false,
    "token": "TOKEN_ID_REFERENCE"
};

describe('event.invite.respond', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.invite.respond'));

    it('should have valid token reference', () => expect(dados.token).to.be.equal(returnData.token));
});
