let expect = require('expect.js');

const dados = {
    "action": "event.route.finish",
    "token": "TOKEN_ID_REFERENCE",
    "data": {
        "socialId": 106477995095735420884
    }
};

const returnData = {
    "error": false,
    "token": "TOKEN_ID_REFERENCE",
    "result": {
        "res": [{
            "data_enviado": "2018-11-03T19:00:27.000Z",
            "trajetoUuid": "337abc68-662d-4f9c-8e8d-4da5b0e9917c",
            "usuario": {
                "id": 7,
                "nome": "outro contato",
                "email": "ee@e.com",
                "imgUrl": null,
                "socialId": "1010",
                "created": false
            }
        }, {
            "data_enviado": "2018-11-03T18:57:33.000Z",
            "trajetoUuid": "34a50ed8-193c-48ab-b637-ebb3b72409de",
            "usuario": {
                "id": 7,
                "nome": "outro contato",
                "email": "ee@e.com",
                "imgUrl": null,
                "socialId": "1010",
                "created": false
            }
        }]
    }
}

describe('event.invite.get', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.invite.get'));

    it('should have valid token reference', () => expect(dados.token).to.be.equal(returnData.token));
});
