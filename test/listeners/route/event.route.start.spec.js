let expect = require('expect.js');

const dados = {
    "action": "event.route.start",
    "token": "UUID_TOKEN_REFERENCE",
    "data": {
        "convidados": [
            {
                "bitmap": null,
                "email": "eliasdarruda@gmail.com",
                "imgUrl": "https://static1.segredosdesalao.com.br/articles/7/13/70/7/@/186717-a-reconstrucao-deixa-o-cabelo-mais-forte-article_media_item-2.jpg",
                "nome": "Elias Arruda",
                "roundedImg": null,
                "socialId": "106477995095735420884",
                "created": false,
                "id": 6
            }
        ],
        "criador": {
            "bitmap": null,
            "email": "eliasdalben@gmail.com",
            "imgUrl": "https://lh6.googleusercontent.com/-DIMMYBjsjxU/AAAAAAAAAAI/AAAAAAAAAH8/a-6hyPLAxOI/s96-c/photo.jpg",
            "nome": "Elias Arruda",
            "roundedImg": null,
            "socialId": "114535956606723975650",
            "created": false,
            "id": 0
        },
        "dataInicio": "11/17/2018 17:12:50",
        "destino": {
            "local": null,
            "latitude": -25.4062047,
            "longitude": -49.2503365
        },
        "partida": {
            "local": null,
            "latitude": -25.3978772,
            "longitude": -49.24599019999999
        },
        "pontos": {
            "fullPathData": {
                "zzdw": [
                    {
                        "latitude": -25.39788,
                        "longitude": -49.245990000000006
                    },
                    {
                        "latitude": -25.399230000000003,
                        "longitude": -49.24723
                    },
                    {
                        "latitude": -25.40041,
                        "longitude": -49.248360000000005
                    },
                    {
                        "latitude": -25.40145,
                        "longitude": -49.24933
                    },
                    {
                        "latitude": -25.40209,
                        "longitude": -49.249880000000005
                    },
                    {
                        "latitude": -25.40341,
                        "longitude": -49.251110000000004
                    },
                    {
                        "latitude": -25.404400000000003,
                        "longitude": -49.252030000000005
                    },
                    {
                        "latitude": -25.404560000000004,
                        "longitude": -49.25222
                    },
                    {
                        "latitude": -25.404790000000002,
                        "longitude": -49.25258
                    },
                    {
                        "latitude": -25.404880000000002,
                        "longitude": -49.252900000000004
                    },
                    {
                        "latitude": -25.40492,
                        "longitude": -49.25314
                    },
                    {
                        "latitude": -25.4051,
                        "longitude": -49.25301
                    },
                    {
                        "latitude": -25.40529,
                        "longitude": -49.25287
                    },
                    {
                        "latitude": -25.405510000000003,
                        "longitude": -49.25264000000001
                    },
                    {
                        "latitude": -25.40586,
                        "longitude": -49.25231
                    },
                    {
                        "latitude": -25.40603,
                        "longitude": -49.25215000000001
                    },
                    {
                        "latitude": -25.406100000000002,
                        "longitude": -49.25208000000001
                    },
                    {
                        "latitude": -25.40518,
                        "longitude": -49.25124
                    },
                    {
                        "latitude": -25.406200000000002,
                        "longitude": -49.25034
                    }
                ],
                "zzeb": {
                    "bitmapDescriptor": null,
                    "zzcm": null,
                    "type": 0
                },
                "zzec": {
                    "bitmapDescriptor": null,
                    "zzcm": null,
                    "type": 0
                },
                "zzee": null,
                "color": -16777216,
                "width": 10,
                "zzcr": 0,
                "zzcs": true,
                "zzct": false,
                "zzdy": false,
                "zzed": 0
            },
            "legData": {
                "arrivalTime": null,
                "departureTime": null,
                "distance": {
                    "humanReadable": "1.5 km",
                    "inMeters": 1523
                },
                "duration": {
                    "humanReadable": "4 mins",
                    "inSeconds": 237
                },
                "durationInTraffic": null,
                "endAddress": "Rua Quintino Bocaiuva, 152 - Cabral, Curitiba - PR, 80035-090, Brazil",
                "endLocation": {
                    "lat": -25.4062047,
                    "lng": -49.2503365
                },
                "startAddress": "R. Jovino do Rosário, 356 - Boa Vista, Curitiba - PR, 82540-115, Brazil",
                "startLocation": {
                    "lat": -25.3978772,
                    "lng": -49.24599019999999
                },
                "steps": [
                    {
                        "distance": {
                            "humanReadable": "0.4 km",
                            "inMeters": 368
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 53
                        },
                        "endLocation": {
                            "lat": -25.4004068,
                            "lng": -49.2483558
                        },
                        "htmlInstructions": "Head <b>southwest</b> on <b>R. Jovino do Rosário</b> toward <b>R. Holanda</b>",
                        "maneuver": null,
                        "polyline": {
                            "points": "vo_zCljqkHl@h@nBbBv@t@v@r@`@^lAjAhB`BPR"
                        },
                        "startLocation": {
                            "lat": -25.3978772,
                            "lng": -49.24599019999999
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "0.6 km",
                            "inMeters": 624
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 61
                        },
                        "endLocation": {
                            "lat": -25.4046707,
                            "lng": -49.2523782
                        },
                        "htmlInstructions": "Continue onto <b>R. Belém</b>",
                        "maneuver": null,
                        "polyline": {
                            "points": "p_`zCfyqkHvApAvBnB~BlBfGtFbBxA`B|APRLPT^"
                        },
                        "startLocation": {
                            "lat": -25.4004068,
                            "lng": -49.2483558
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "83 m",
                            "inMeters": 83
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 9
                        },
                        "endLocation": {
                            "lat": -25.4049192,
                            "lng": -49.2531423
                        },
                        "htmlInstructions": "Continue onto <b>R. Cel. João Alençar Guimarães Filho</b>",
                        "maneuver": null,
                        "polyline": {
                            "points": "dz`zCjrrkHLRDHBHDJBNBPBPFn@"
                        },
                        "startLocation": {
                            "lat": -25.4046707,
                            "lng": -49.2523782
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "0.2 km",
                            "inMeters": 151
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 32
                        },
                        "endLocation": {
                            "lat": -25.4059752,
                            "lng": -49.2521973
                        },
                        "htmlInstructions": "Turn <b>left</b> onto <b>R. dos Funcionários</b>",
                        "maneuver": "turn-left",
                        "polyline": {
                            "points": "v{`zCbwrkHb@Yd@[j@m@p@m@RSVU"
                        },
                        "startLocation": {
                            "lat": -25.4049192,
                            "lng": -49.2531423
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "0.2 km",
                            "inMeters": 151
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 34
                        },
                        "endLocation": {
                            "lat": -25.4051783,
                            "lng": -49.2512351
                        },
                        "htmlInstructions": "Turn <b>left</b> onto <b>Av. Paraná</b>",
                        "maneuver": "turn-left",
                        "polyline": {
                            "points": "jbazCfqrkHHI@AJK{AsA{AsA"
                        },
                        "startLocation": {
                            "lat": -25.4059752,
                            "lng": -49.2521973
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "0.1 km",
                            "inMeters": 146
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 48
                        },
                        "endLocation": {
                            "lat": -25.4062047,
                            "lng": -49.2503365
                        },
                        "htmlInstructions": "Turn <b>right</b> onto <b>Rua Quintino Bocaiuva</b><div style=\"font-size:0.9em\">Destination will be on the right</div>",
                        "maneuver": "turn-right",
                        "polyline": {
                            "points": "j}`zCfkrkHhA_AfAaAx@q@"
                        },
                        "startLocation": {
                            "lat": -25.4051783,
                            "lng": -49.2512351
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    }
                ]
            },
            "mainTravelModeData": "DRIVING",
            "pontosList": [
                [
                    {
                        "latitude": -25.39788,
                        "longitude": -49.245990000000006
                    },
                    {
                        "latitude": -25.399230000000003,
                        "longitude": -49.24723
                    },
                    {
                        "latitude": -25.40041,
                        "longitude": -49.248360000000005
                    },
                    {
                        "latitude": -25.40145,
                        "longitude": -49.24933
                    },
                    {
                        "latitude": -25.40209,
                        "longitude": -49.249880000000005
                    },
                    {
                        "latitude": -25.40341,
                        "longitude": -49.251110000000004
                    },
                    {
                        "latitude": -25.404400000000003,
                        "longitude": -49.252030000000005
                    },
                    {
                        "latitude": -25.404560000000004,
                        "longitude": -49.25222
                    },
                    {
                        "latitude": -25.404790000000002,
                        "longitude": -49.25258
                    },
                    {
                        "latitude": -25.404880000000002,
                        "longitude": -49.252900000000004
                    },
                    {
                        "latitude": -25.40492,
                        "longitude": -49.25314
                    },
                    {
                        "latitude": -25.4051,
                        "longitude": -49.25301
                    },
                    {
                        "latitude": -25.40529,
                        "longitude": -49.25287
                    },
                    {
                        "latitude": -25.405510000000003,
                        "longitude": -49.25264000000001
                    },
                    {
                        "latitude": -25.40586,
                        "longitude": -49.25231
                    },
                    {
                        "latitude": -25.40603,
                        "longitude": -49.25215000000001
                    },
                    {
                        "latitude": -25.406100000000002,
                        "longitude": -49.25208000000001
                    },
                    {
                        "latitude": -25.40518,
                        "longitude": -49.25124
                    },
                    {
                        "latitude": -25.406200000000002,
                        "longitude": -49.25034
                    }
                ]
            ],
            "travelModeData": [
                "DRIVING"
            ]
        },
        "tipoTrajeto": {
            "descricao": null,
            "modo": "driving"
        },
        "uuid": null,
        "bateria": 59,
        "duracao": 237,
        "finalizado": false
    }
};

// E da broadcast dos convites

const returnData = {
    "error": false,
    "token": "UUID_TOKEN_REFERENCE",
    "result": {
        "dataInicio": "2018-11-17T19:14:34.220Z",
        "uuid": "ba0af79b-62f8-4833-ae60-b1479abd55d4",
        "bateria": 59,
        "duracao": 237,
        "finalizado": false,
        "pontos": {
            "fullPathData": {
                "zzdw": [
                    {
                        "latitude": -25.39788,
                        "longitude": -49.245990000000006
                    },
                    {
                        "latitude": -25.399230000000003,
                        "longitude": -49.24723
                    },
                    {
                        "latitude": -25.40041,
                        "longitude": -49.248360000000005
                    },
                    {
                        "latitude": -25.40145,
                        "longitude": -49.24933
                    },
                    {
                        "latitude": -25.40209,
                        "longitude": -49.249880000000005
                    },
                    {
                        "latitude": -25.40341,
                        "longitude": -49.251110000000004
                    },
                    {
                        "latitude": -25.404400000000003,
                        "longitude": -49.252030000000005
                    },
                    {
                        "latitude": -25.404560000000004,
                        "longitude": -49.25222
                    },
                    {
                        "latitude": -25.404790000000002,
                        "longitude": -49.25258
                    },
                    {
                        "latitude": -25.404880000000002,
                        "longitude": -49.252900000000004
                    },
                    {
                        "latitude": -25.40492,
                        "longitude": -49.25314
                    },
                    {
                        "latitude": -25.4051,
                        "longitude": -49.25301
                    },
                    {
                        "latitude": -25.40529,
                        "longitude": -49.25287
                    },
                    {
                        "latitude": -25.405510000000003,
                        "longitude": -49.25264000000001
                    },
                    {
                        "latitude": -25.40586,
                        "longitude": -49.25231
                    },
                    {
                        "latitude": -25.40603,
                        "longitude": -49.25215000000001
                    },
                    {
                        "latitude": -25.406100000000002,
                        "longitude": -49.25208000000001
                    },
                    {
                        "latitude": -25.40518,
                        "longitude": -49.25124
                    },
                    {
                        "latitude": -25.406200000000002,
                        "longitude": -49.25034
                    }
                ],
                "zzeb": {
                    "bitmapDescriptor": null,
                    "zzcm": null,
                    "type": 0
                },
                "zzec": {
                    "bitmapDescriptor": null,
                    "zzcm": null,
                    "type": 0
                },
                "zzee": null,
                "color": -16777216,
                "width": 10,
                "zzcr": 0,
                "zzcs": true,
                "zzct": false,
                "zzdy": false,
                "zzed": 0
            },
            "legData": {
                "arrivalTime": null,
                "departureTime": null,
                "distance": {
                    "humanReadable": "1.5 km",
                    "inMeters": 1523
                },
                "duration": {
                    "humanReadable": "4 mins",
                    "inSeconds": 237
                },
                "durationInTraffic": null,
                "endAddress": "Rua Quintino Bocaiuva, 152 - Cabral, Curitiba - PR, 80035-090, Brazil",
                "endLocation": {
                    "lat": -25.4062047,
                    "lng": -49.2503365
                },
                "startAddress": "R. Jovino do Rosário, 356 - Boa Vista, Curitiba - PR, 82540-115, Brazil",
                "startLocation": {
                    "lat": -25.3978772,
                    "lng": -49.24599019999999
                },
                "steps": [
                    {
                        "distance": {
                            "humanReadable": "0.4 km",
                            "inMeters": 368
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 53
                        },
                        "endLocation": {
                            "lat": -25.4004068,
                            "lng": -49.2483558
                        },
                        "htmlInstructions": "Head <b>southwest</b> on <b>R. Jovino do Rosário</b> toward <b>R. Holanda</b>",
                        "maneuver": null,
                        "polyline": {
                            "points": "vo_zCljqkHl@h@nBbBv@t@v@r@`@^lAjAhB`BPR"
                        },
                        "startLocation": {
                            "lat": -25.3978772,
                            "lng": -49.24599019999999
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "0.6 km",
                            "inMeters": 624
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 61
                        },
                        "endLocation": {
                            "lat": -25.4046707,
                            "lng": -49.2523782
                        },
                        "htmlInstructions": "Continue onto <b>R. Belém</b>",
                        "maneuver": null,
                        "polyline": {
                            "points": "p_`zCfyqkHvApAvBnB~BlBfGtFbBxA`B|APRLPT^"
                        },
                        "startLocation": {
                            "lat": -25.4004068,
                            "lng": -49.2483558
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "83 m",
                            "inMeters": 83
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 9
                        },
                        "endLocation": {
                            "lat": -25.4049192,
                            "lng": -49.2531423
                        },
                        "htmlInstructions": "Continue onto <b>R. Cel. João Alençar Guimarães Filho</b>",
                        "maneuver": null,
                        "polyline": {
                            "points": "dz`zCjrrkHLRDHBHDJBNBPBPFn@"
                        },
                        "startLocation": {
                            "lat": -25.4046707,
                            "lng": -49.2523782
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "0.2 km",
                            "inMeters": 151
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 32
                        },
                        "endLocation": {
                            "lat": -25.4059752,
                            "lng": -49.2521973
                        },
                        "htmlInstructions": "Turn <b>left</b> onto <b>R. dos Funcionários</b>",
                        "maneuver": "turn-left",
                        "polyline": {
                            "points": "v{`zCbwrkHb@Yd@[j@m@p@m@RSVU"
                        },
                        "startLocation": {
                            "lat": -25.4049192,
                            "lng": -49.2531423
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "0.2 km",
                            "inMeters": 151
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 34
                        },
                        "endLocation": {
                            "lat": -25.4051783,
                            "lng": -49.2512351
                        },
                        "htmlInstructions": "Turn <b>left</b> onto <b>Av. Paraná</b>",
                        "maneuver": "turn-left",
                        "polyline": {
                            "points": "jbazCfqrkHHI@AJK{AsA{AsA"
                        },
                        "startLocation": {
                            "lat": -25.4059752,
                            "lng": -49.2521973
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    },
                    {
                        "distance": {
                            "humanReadable": "0.1 km",
                            "inMeters": 146
                        },
                        "duration": {
                            "humanReadable": "1 min",
                            "inSeconds": 48
                        },
                        "endLocation": {
                            "lat": -25.4062047,
                            "lng": -49.2503365
                        },
                        "htmlInstructions": "Turn <b>right</b> onto <b>Rua Quintino Bocaiuva</b><div style=\"font-size:0.9em\">Destination will be on the right</div>",
                        "maneuver": "turn-right",
                        "polyline": {
                            "points": "j}`zCfkrkHhA_AfAaAx@q@"
                        },
                        "startLocation": {
                            "lat": -25.4051783,
                            "lng": -49.2512351
                        },
                        "steps": null,
                        "transitDetails": null,
                        "travelMode": "DRIVING"
                    }
                ]
            },
            "mainTravelModeData": "DRIVING",
            "pontosList": [
                [
                    {
                        "latitude": -25.39788,
                        "longitude": -49.245990000000006
                    },
                    {
                        "latitude": -25.399230000000003,
                        "longitude": -49.24723
                    },
                    {
                        "latitude": -25.40041,
                        "longitude": -49.248360000000005
                    },
                    {
                        "latitude": -25.40145,
                        "longitude": -49.24933
                    },
                    {
                        "latitude": -25.40209,
                        "longitude": -49.249880000000005
                    },
                    {
                        "latitude": -25.40341,
                        "longitude": -49.251110000000004
                    },
                    {
                        "latitude": -25.404400000000003,
                        "longitude": -49.252030000000005
                    },
                    {
                        "latitude": -25.404560000000004,
                        "longitude": -49.25222
                    },
                    {
                        "latitude": -25.404790000000002,
                        "longitude": -49.25258
                    },
                    {
                        "latitude": -25.404880000000002,
                        "longitude": -49.252900000000004
                    },
                    {
                        "latitude": -25.40492,
                        "longitude": -49.25314
                    },
                    {
                        "latitude": -25.4051,
                        "longitude": -49.25301
                    },
                    {
                        "latitude": -25.40529,
                        "longitude": -49.25287
                    },
                    {
                        "latitude": -25.405510000000003,
                        "longitude": -49.25264000000001
                    },
                    {
                        "latitude": -25.40586,
                        "longitude": -49.25231
                    },
                    {
                        "latitude": -25.40603,
                        "longitude": -49.25215000000001
                    },
                    {
                        "latitude": -25.406100000000002,
                        "longitude": -49.25208000000001
                    },
                    {
                        "latitude": -25.40518,
                        "longitude": -49.25124
                    },
                    {
                        "latitude": -25.406200000000002,
                        "longitude": -49.25034
                    }
                ],
                [
                    {
                        "latitude": -25.39788,
                        "longitude": -49.245990000000006
                    },
                    {
                        "latitude": -25.399230000000003,
                        "longitude": -49.24723
                    },
                    {
                        "latitude": -25.40041,
                        "longitude": -49.248360000000005
                    },
                    {
                        "latitude": -25.40145,
                        "longitude": -49.24933
                    },
                    {
                        "latitude": -25.40209,
                        "longitude": -49.249880000000005
                    },
                    {
                        "latitude": -25.402700000000003,
                        "longitude": -49.249230000000004
                    },
                    {
                        "latitude": -25.402790000000003,
                        "longitude": -49.249140000000004
                    },
                    {
                        "latitude": -25.403180000000003,
                        "longitude": -49.24875
                    },
                    {
                        "latitude": -25.40407,
                        "longitude": -49.24786
                    },
                    {
                        "latitude": -25.40461,
                        "longitude": -49.247310000000006
                    },
                    {
                        "latitude": -25.405340000000002,
                        "longitude": -49.247420000000005
                    },
                    {
                        "latitude": -25.405590000000004,
                        "longitude": -49.24745
                    },
                    {
                        "latitude": -25.405710000000003,
                        "longitude": -49.24748
                    },
                    {
                        "latitude": -25.405980000000003,
                        "longitude": -49.247580000000006
                    },
                    {
                        "latitude": -25.406190000000002,
                        "longitude": -49.24774000000001
                    },
                    {
                        "latitude": -25.406760000000002,
                        "longitude": -49.248160000000006
                    },
                    {
                        "latitude": -25.407000000000004,
                        "longitude": -49.248400000000004
                    },
                    {
                        "latitude": -25.407100000000003,
                        "longitude": -49.24864
                    },
                    {
                        "latitude": -25.40717,
                        "longitude": -49.24902
                    },
                    {
                        "latitude": -25.407210000000003,
                        "longitude": -49.24917000000001
                    },
                    {
                        "latitude": -25.407310000000003,
                        "longitude": -49.24936
                    },
                    {
                        "latitude": -25.406200000000002,
                        "longitude": -49.25034
                    }
                ],
                [
                    {
                        "latitude": -25.39788,
                        "longitude": -49.245990000000006
                    },
                    {
                        "latitude": -25.399230000000003,
                        "longitude": -49.24723
                    },
                    {
                        "latitude": -25.399790000000003,
                        "longitude": -49.24777
                    },
                    {
                        "latitude": -25.4004,
                        "longitude": -49.24696
                    },
                    {
                        "latitude": -25.400460000000002,
                        "longitude": -49.246900000000004
                    },
                    {
                        "latitude": -25.40106,
                        "longitude": -49.246080000000006
                    },
                    {
                        "latitude": -25.40152,
                        "longitude": -49.245520000000006
                    },
                    {
                        "latitude": -25.4021,
                        "longitude": -49.244910000000004
                    },
                    {
                        "latitude": -25.402230000000003,
                        "longitude": -49.24477
                    },
                    {
                        "latitude": -25.402800000000003,
                        "longitude": -49.24421
                    },
                    {
                        "latitude": -25.40398,
                        "longitude": -49.24602
                    },
                    {
                        "latitude": -25.40397,
                        "longitude": -49.24609
                    },
                    {
                        "latitude": -25.40397,
                        "longitude": -49.246170000000006
                    },
                    {
                        "latitude": -25.40397,
                        "longitude": -49.24633000000001
                    },
                    {
                        "latitude": -25.404010000000003,
                        "longitude": -49.24647
                    },
                    {
                        "latitude": -25.404210000000003,
                        "longitude": -49.24692
                    },
                    {
                        "latitude": -25.40436,
                        "longitude": -49.24723
                    },
                    {
                        "latitude": -25.404370000000004,
                        "longitude": -49.24725
                    },
                    {
                        "latitude": -25.404380000000003,
                        "longitude": -49.24727000000001
                    },
                    {
                        "latitude": -25.40452,
                        "longitude": -49.2473
                    },
                    {
                        "latitude": -25.405340000000002,
                        "longitude": -49.247420000000005
                    },
                    {
                        "latitude": -25.405590000000004,
                        "longitude": -49.24745
                    },
                    {
                        "latitude": -25.405980000000003,
                        "longitude": -49.247580000000006
                    },
                    {
                        "latitude": -25.406760000000002,
                        "longitude": -49.248160000000006
                    },
                    {
                        "latitude": -25.407000000000004,
                        "longitude": -49.248400000000004
                    },
                    {
                        "latitude": -25.40705,
                        "longitude": -49.24851
                    },
                    {
                        "latitude": -25.407100000000003,
                        "longitude": -49.24864
                    },
                    {
                        "latitude": -25.40717,
                        "longitude": -49.24902
                    },
                    {
                        "latitude": -25.407210000000003,
                        "longitude": -49.24917000000001
                    },
                    {
                        "latitude": -25.407310000000003,
                        "longitude": -49.24936
                    },
                    {
                        "latitude": -25.406200000000002,
                        "longitude": -49.25034
                    }
                ]
            ],
            "travelModeData": [
                "DRIVING"
            ]
        },
        "tipoTrajeto": {
            "descricao": null,
            "modo": "driving"
        },
        "partida": {
            "latitude": -25.3978772,
            "longitude": -49.24599019999999,
            "local": null
        },
        "destino": {
            "latitude": -25.4062047,
            "longitude": -49.2503365,
            "local": null
        },
        "criador": {
            "id": 0,
            "nome": "Elias Arruda",
            "email": "eliasdalben@gmail.com",
            "created": false
        }
    }
}

describe('event.route.finish', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.route.start'));

    it('should have valid token reference', () => expect(dados.token).to.be.equal(returnData.token));

    it('should have valid uuid', () => expect(returnData['result'].uuid).to.be.true);
});
