let expect = require('expect.js');

const dados = {
    "action": "event.panic.add",
    "token": 421412,
    "data": {
        "socialId": 106477995095735420884,
        "coordenada": {
            "latitude": 11.224,
            "longitude": 22.55
        },
        "trajetoUuid": "34a50ed8-193c-48ab-b637-ebb3b72409de"
    }
};

const returnData = {
    "error": false,
    "token": "TOKEN_ID_REFERENCE"
};

describe('event.panic.add', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.panic.add'));

    it('should have valid token reference', () => expect(dados.token).to.be.equal(returnData.token));
});
