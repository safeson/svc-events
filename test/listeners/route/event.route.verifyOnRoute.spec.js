let expect = require('expect.js');

const dados = {
    "action": "event.route.verifyOnRoute",
    "token": "a92b9802-7790-451f-889b-2d1b71bc9f62",
    "data": {
        "coordenada": {
            "latitude": 42.11,
            "longidute": 11.22
        },
        "data": new Date(),
        "trajetoUuid": "8cf7e692-5d74-455e-a1d8-ce06940ae696",
        "onRoute": true
    }
};

const returnData = {
    "error": false,
    "token": "a92b9802-7790-451f-889b-2d1b71bc9f62",
    "result": {
    }
}

describe('event.route.verifyOnRoute', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.route.verifyOnRoute'));

    it('should have valid token reference', () => expect(dados.token).to.be.equal(returnData.token));

    it('should have valid array of objects', () => expect(returnData['result'].res).to.not.be.empty);
});
