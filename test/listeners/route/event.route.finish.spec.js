let expect = require('expect.js');

const dados = {
    "action": "event.route.finish",
    "token": "TOKEN_ID_REFERENCE",
    "data": {
        "bateria": 11,
        "tipoFim": "CONCLUIDO"
    }
};

const returnData = {
    "error": false,
    "broadcast": true,
    "event": "panic.add",
    "result": {
        "id": 1,
        "coordenada": {
            "latitude": 11.224,
            "longitude": 22.55
        },
        "trajetoUuid": "34a50ed8-193c-48ab-b637-ebb3b72409de",
        "data": "2018-11-10T15:13:18.626Z",
        "socialId": 106477995095735420000
    }
};

describe('event.route.finish', () => {
    it('should have data', () => expect(dados.data).to.not.be.undefined);
    it('should have action', () => expect(dados.action).to.be.equal('event.route.finish'));

    it('should have valid token reference', () => expect(dados.token).to.be.equal(returnData.token));
});
