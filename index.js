

let IoC = require('electrolyte');

function App(callback) {
	IoC.use(IoC.dir('app'));
	IoC.use(IoC.dir('app/configs'));
	IoC.use(IoC.dir('app/listeners'));
	IoC.use(IoC.dir('app/components'));
	IoC.use(IoC.dir('app/components/mysql'));
	IoC.use(IoC.dir('app/components/messages'));
	IoC.use(IoC.dir('app/components/listeners'));
	IoC.use(IoC.dir('app/components/maps'));
	IoC.use(IoC.node_modules());
	
	IoC.create('app').then(callback, callback);
}

exports = module.exports = App;

new App(function(err) {
	if (err) {
		return console.log('Erro criando o APP - ' + err);
	}

	console.log('App criado com sucesso');
});