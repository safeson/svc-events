CREATE DATABASE  IF NOT EXISTS `tcc` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tcc`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: tcc
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alerta`
--

DROP TABLE IF EXISTS `alerta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alerta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(500) DEFAULT NULL COMMENT 'Descrição do alerta por exemplo, indicando que ocorreu um assalto e as caracteristicas do assaltante',
  `data_criacao` timestamp NULL DEFAULT NULL COMMENT 'Data da criação do alerta\n',
  `usuario_id` int(11) NOT NULL,
  `tipo_alerta_id` int(11) NOT NULL,
  `coordenada_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_alerta_usuario1_idx` (`usuario_id`),
  KEY `fk_alerta_tipo_alerta1_idx` (`tipo_alerta_id`),
  KEY `fk_alerta_coordenada1_idx` (`coordenada_id`),
  CONSTRAINT `fk_alerta_coordenada1` FOREIGN KEY (`coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alerta_tipo_alerta1` FOREIGN KEY (`tipo_alerta_id`) REFERENCES `tipo_alerta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_alerta_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alerta`
--

LOCK TABLES `alerta` WRITE;
/*!40000 ALTER TABLE `alerta` DISABLE KEYS */;
/*!40000 ALTER TABLE `alerta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `usuario_id` int(11) NOT NULL,
  `contato_id` int(11) NOT NULL,
  PRIMARY KEY (`contato_id`,`usuario_id`),
  KEY `fk_contato_usuario1_idx` (`usuario_id`),
  KEY `fk_contato_usuario2_idx` (`contato_id`),
  CONSTRAINT `fk_contato_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contato_usuario2` FOREIGN KEY (`contato_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coordenada`
--

DROP TABLE IF EXISTS `coordenada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coordenada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `local` varchar(255) DEFAULT NULL COMMENT 'Local, endereço, nome do lugar',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coordenada`
--

LOCK TABLES `coordenada` WRITE;
/*!40000 ALTER TABLE `coordenada` DISABLE KEYS */;
/*!40000 ALTER TABLE `coordenada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorito`
--

DROP TABLE IF EXISTS `favorito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `coordenada_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_favorito_coordenada1_idx` (`coordenada_id`),
  KEY `fk_favorito_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_favorito_coordenada1` FOREIGN KEY (`coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_favorito_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorito`
--

LOCK TABLES `favorito` WRITE;
/*!40000 ALTER TABLE `favorito` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL COMMENT 'Nome identificador do grupo (ex: familia)',
  `usuario_id` int(11) NOT NULL COMMENT 'Criador do grupo\n\n',
  PRIMARY KEY (`id`),
  KEY `fk_grupo_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_grupo_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panico`
--

DROP TABLE IF EXISTS `panico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `panico` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT NULL COMMENT 'Data que foi acionado o panico',
  `usuario_id` int(11) NOT NULL,
  `coordenada_id` int(11) NOT NULL,
  `trajeto_uuid` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_panico_usuario1_idx` (`usuario_id`),
  KEY `fk_panico_coordenada1_idx` (`coordenada_id`),
  KEY `fk_panico_trajeto1_idx` (`trajeto_uuid`),
  CONSTRAINT `fk_panico_coordenada1` FOREIGN KEY (`coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_panico_trajeto1` FOREIGN KEY (`trajeto_uuid`) REFERENCES `trajeto` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_panico_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panico`
--

LOCK TABLES `panico` WRITE;
/*!40000 ALTER TABLE `panico` DISABLE KEYS */;
/*!40000 ALTER TABLE `panico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regiao_perigo`
--

DROP TABLE IF EXISTS `regiao_perigo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regiao_perigo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `raio` double DEFAULT NULL COMMENT 'Raio da região de perigo',
  `intensidade` double DEFAULT NULL COMMENT 'Intensidade calculada do nivel de perigo',
  `localidade` varchar(255) DEFAULT NULL COMMENT 'Descrição da regiao, bairro, localidade',
  `coordenada_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_regiao_perigo_coordenada1_idx` (`coordenada_id`),
  CONSTRAINT `fk_regiao_perigo_coordenada1` FOREIGN KEY (`coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regiao_perigo`
--

LOCK TABLES `regiao_perigo` WRITE;
/*!40000 ALTER TABLE `regiao_perigo` DISABLE KEYS */;
/*!40000 ALTER TABLE `regiao_perigo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regiao_perigo_alerta`
--

DROP TABLE IF EXISTS `regiao_perigo_alerta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regiao_perigo_alerta` (
  `alerta_id` int(11) NOT NULL,
  `regiao_perigo_id` int(11) NOT NULL,
  PRIMARY KEY (`alerta_id`,`regiao_perigo_id`),
  KEY `fk_regiao_perigo_alerta_alerta1_idx` (`alerta_id`),
  KEY `fk_regiao_perigo_alerta_regiao_perigo1_idx` (`regiao_perigo_id`),
  CONSTRAINT `fk_regiao_perigo_alerta_alerta1` FOREIGN KEY (`alerta_id`) REFERENCES `alerta` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_regiao_perigo_alerta_regiao_perigo1` FOREIGN KEY (`regiao_perigo_id`) REFERENCES `regiao_perigo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regiao_perigo_alerta`
--

LOCK TABLES `regiao_perigo_alerta` WRITE;
/*!40000 ALTER TABLE `regiao_perigo_alerta` DISABLE KEYS */;
/*!40000 ALTER TABLE `regiao_perigo_alerta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_alerta`
--

DROP TABLE IF EXISTS `tipo_alerta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_alerta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL COMMENT 'Nome do tipo por exemplo: Assaltante',
  `gravidade` int(11) DEFAULT NULL COMMENT 'Gravidade do alerta (numero fixo, de 1 a 5 por exemplo)',
  `expiracao` int(11) DEFAULT NULL COMMENT 'Tempo que o alerta irá expirar em minutos',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_alerta`
--

LOCK TABLES `tipo_alerta` WRITE;
/*!40000 ALTER TABLE `tipo_alerta` DISABLE KEYS */;
INSERT INTO `tipo_alerta` VALUES (1,'Assalto',4,60),(2,'Furto',1,20),(3,'Assédio sexual',3,30),(4,'Sequestro',5,120),(5,'Animal Perigoso',1,30),(6,'Pouca iluminação',2,20);
/*!40000 ALTER TABLE `tipo_alerta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_fim_trajeto`
--

DROP TABLE IF EXISTS `tipo_fim_trajeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_fim_trajeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL COMMENT 'Descrição do fim do trajeto (ex: concluido, desligado, interrompido, cancelado)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_fim_trajeto`
--

LOCK TABLES `tipo_fim_trajeto` WRITE;
/*!40000 ALTER TABLE `tipo_fim_trajeto` DISABLE KEYS */;
INSERT INTO `tipo_fim_trajeto` VALUES (1,'Concluido'),(2,'Cancelado'),(3,'Conexao perdida'),(4,'Interrompido');
/*!40000 ALTER TABLE `tipo_fim_trajeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_trajeto`
--

DROP TABLE IF EXISTS `tipo_trajeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_trajeto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(45) DEFAULT NULL COMMENT 'Tipo do trajeto (ex: Andando, dirigindo, etc..)',
  `modo` varchar(45) DEFAULT NULL COMMENT 'Modo de locomoção ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_trajeto`
--

LOCK TABLES `tipo_trajeto` WRITE;
/*!40000 ALTER TABLE `tipo_trajeto` DISABLE KEYS */;
INSERT INTO `tipo_trajeto` VALUES (1,'Andando','WALKING'),(2,'Dirigindo','DRIVING'),(3,'Bicicleta','BICYCLING'),(4,'Em Transito','TRANSIT'),(5,'Onibus','TRANSIT'),(6,'Metro','TRANSIT'),(7,'Trem','TRANSIT');
/*!40000 ALTER TABLE `tipo_trajeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trajeto`
--

DROP TABLE IF EXISTS `trajeto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trajeto` (
  `uuid` varchar(45) NOT NULL,
  `data_inicio` timestamp NULL DEFAULT NULL,
  `bateria_inicio` double DEFAULT NULL,
  `bateria_baixa` tinyint(4) DEFAULT NULL,
  `avaliacao` int(11) DEFAULT NULL,
  `distancia` double DEFAULT NULL COMMENT 'Em metros',
  `duracao_estimada` int(11) DEFAULT NULL COMMENT 'Em segundos',
  `pontos` varchar(10000) DEFAULT NULL COMMENT 'Uma array em JSON dos pontos que compoem a rota\n',
  `usuario_id` int(11) NOT NULL,
  `partida_coordenada_id` int(11) NOT NULL,
  `destino_coordenada_id` int(11) NOT NULL,
  `tipo_trajeto_id` int(11) NOT NULL,
  `fim_trajeto_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `fk_trajeto_usuario_idx` (`usuario_id`),
  KEY `fk_trajeto_coordenada1_idx` (`partida_coordenada_id`),
  KEY `fk_trajeto_coordenada2_idx` (`destino_coordenada_id`),
  KEY `fk_trajeto_tipo_trajeto1_idx` (`tipo_trajeto_id`),
  KEY `fk_trajeto_fim_trajeto2_idx` (`fim_trajeto_id`),
  CONSTRAINT `fk_trajeto_coordenada1` FOREIGN KEY (`partida_coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_trajeto_coordenada2` FOREIGN KEY (`destino_coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_trajeto_fim_trajeto2` FOREIGN KEY (`fim_trajeto_id`) REFERENCES `trajeto_fim` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_trajeto_tipo_trajeto1` FOREIGN KEY (`tipo_trajeto_id`) REFERENCES `tipo_trajeto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_trajeto_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trajeto`
--

LOCK TABLES `trajeto` WRITE;
/*!40000 ALTER TABLE `trajeto` DISABLE KEYS */;
/*!40000 ALTER TABLE `trajeto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trajeto_convite`
--

DROP TABLE IF EXISTS `trajeto_convite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trajeto_convite` (
  `trajeto_uuid` varchar(45) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `data_enviado` timestamp NULL DEFAULT NULL,
  `data_aceito` timestamp NULL DEFAULT NULL,
  `data_recusado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`trajeto_uuid`,`usuario_id`),
  KEY `fk_trajeto_convite_trajeto1_idx` (`trajeto_uuid`),
  KEY `fk_trajeto_convite_usuario1_idx` (`usuario_id`),
  CONSTRAINT `fk_trajeto_convite_trajeto1` FOREIGN KEY (`trajeto_uuid`) REFERENCES `trajeto` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_trajeto_convite_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trajeto_convite`
--

LOCK TABLES `trajeto_convite` WRITE;
/*!40000 ALTER TABLE `trajeto_convite` DISABLE KEYS */;
/*!40000 ALTER TABLE `trajeto_convite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trajeto_fim`
--

DROP TABLE IF EXISTS `trajeto_fim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trajeto_fim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` timestamp NULL DEFAULT NULL COMMENT 'Caso o trajeto tenha sido cancelado',
  `bateria` double DEFAULT NULL COMMENT 'Porcentagem da bateria no fim do trajeto\n',
  `qtd_assistindo` int(11) DEFAULT NULL COMMENT 'Quantidade de pessoas assistindo ao fim do trajeto',
  `tipo_fim_trajeto_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fim_trajeto_tipo_fim_trajeto1_idx` (`tipo_fim_trajeto_id`),
  CONSTRAINT `fk_fim_trajeto_tipo_fim_trajeto1` FOREIGN KEY (`tipo_fim_trajeto_id`) REFERENCES `tipo_fim_trajeto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trajeto_fim`
--

LOCK TABLES `trajeto_fim` WRITE;
/*!40000 ALTER TABLE `trajeto_fim` DISABLE KEYS */;
/*!40000 ALTER TABLE `trajeto_fim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trajeto_instrucao`
--

DROP TABLE IF EXISTS `trajeto_instrucao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trajeto_instrucao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(100) DEFAULT NULL COMMENT 'Descrição da instrução, ex: vire para esquerda em 150m',
  `distancia` double DEFAULT NULL COMMENT 'Distancia em metros até o fim da instrução',
  `transito` varchar(255) DEFAULT NULL COMMENT 'Contains transit specific information, such as the arrival and departure times, and the name of the transit line.',
  `inicio_coordenada_id` int(11) NOT NULL COMMENT 'Inicio do ponto de instrução',
  `fim_coordenada_id` int(11) NOT NULL COMMENT 'Fim do ponto de instrução',
  `trajeto_uuid` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_instrucao_trajeto_trajeto1_idx` (`trajeto_uuid`),
  KEY `fk_instrucao_trajeto_coordenada1_idx` (`inicio_coordenada_id`),
  KEY `fk_instrucao_trajeto_coordenada2_idx` (`fim_coordenada_id`),
  CONSTRAINT `fk_instrucao_trajeto_coordenada1` FOREIGN KEY (`inicio_coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instrucao_trajeto_coordenada2` FOREIGN KEY (`fim_coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_instrucao_trajeto_trajeto1` FOREIGN KEY (`trajeto_uuid`) REFERENCES `trajeto` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trajeto_instrucao`
--

LOCK TABLES `trajeto_instrucao` WRITE;
/*!40000 ALTER TABLE `trajeto_instrucao` DISABLE KEYS */;
/*!40000 ALTER TABLE `trajeto_instrucao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trajeto_observador`
--

DROP TABLE IF EXISTS `trajeto_observador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trajeto_observador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guest_ip` varchar(45) DEFAULT NULL COMMENT 'Usuário guest IP',
  `guest_agent` varchar(255) DEFAULT NULL COMMENT 'Usuário guest agent',
  `ativo` tinyint(4) DEFAULT '1' COMMENT 'Esconder para a pessoa que está observando o percurso',
  `usuario_id` int(11) DEFAULT NULL COMMENT 'Pessoa que está acompanhando o percurso',
  `data_conectado` timestamp NULL DEFAULT NULL,
  `data_desconectado` timestamp NULL DEFAULT NULL,
  `trajeto_uuid` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_observador_usuario1_idx` (`usuario_id`),
  KEY `fk_observador_trajeto1_idx` (`trajeto_uuid`),
  CONSTRAINT `fk_observador_trajeto1` FOREIGN KEY (`trajeto_uuid`) REFERENCES `trajeto` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_observador_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trajeto_observador`
--

LOCK TABLES `trajeto_observador` WRITE;
/*!40000 ALTER TABLE `trajeto_observador` DISABLE KEYS */;
/*!40000 ALTER TABLE `trajeto_observador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trajeto_verificador`
--

DROP TABLE IF EXISTS `trajeto_verificador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trajeto_verificador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_saida` timestamp NULL DEFAULT NULL COMMENT 'Data que saiu do percurso',
  `data_volta` timestamp NULL DEFAULT NULL COMMENT 'Data que voltou para o percurso',
  `data_parada` timestamp NULL DEFAULT NULL COMMENT 'Data que alertou que ficou tempo demais parado',
  `trajeto_uuid` varchar(45) NOT NULL,
  `coordenada_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_verificador_trajeto_trajeto1_idx` (`trajeto_uuid`),
  KEY `fk_verificador_trajeto_coordenada1_idx` (`coordenada_id`),
  CONSTRAINT `fk_verificador_trajeto_coordenada1` FOREIGN KEY (`coordenada_id`) REFERENCES `coordenada` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_verificador_trajeto_trajeto1` FOREIGN KEY (`trajeto_uuid`) REFERENCES `trajeto` (`uuid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trajeto_verificador`
--

LOCK TABLES `trajeto_verificador` WRITE;
/*!40000 ALTER TABLE `trajeto_verificador` DISABLE KEYS */;
/*!40000 ALTER TABLE `trajeto_verificador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `id_social` varchar(45) DEFAULT NULL COMMENT 'Id da rede social (facebook, google+)',
  `cpf` varchar(14) DEFAULT NULL,
  `data_inclusao` timestamp NULL DEFAULT NULL,
  `data_exclusao` timestamp NULL DEFAULT NULL,
  `data_bloqueado` timestamp NULL DEFAULT NULL,
  `celular` varchar(25) DEFAULT NULL,
  `celular_responsavel` varchar(25) DEFAULT NULL,
  `cep` varchar(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `img_url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_grupo`
--

DROP TABLE IF EXISTS `usuario_grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_grupo` (
  `usuario_id` int(11) NOT NULL,
  `grupo_id` int(11) NOT NULL,
  KEY `fk_usuario_grupo_usuario1_idx` (`usuario_id`),
  KEY `fk_usuario_grupo_grupo1_idx` (`grupo_id`),
  CONSTRAINT `fk_usuario_grupo_grupo1` FOREIGN KEY (`grupo_id`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_grupo_usuario1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_grupo`
--

LOCK TABLES `usuario_grupo` WRITE;
/*!40000 ALTER TABLE `usuario_grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario_grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tcc'
--

--
-- Dumping routines for database 'tcc'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-28 20:23:55
